<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;

//custom
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UsersExport implements FromQuery, WithHeadings,WithMapping,WithColumnFormatting
{
	use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    // 	return User::all();
    // }

    public function __construct(int $year)
    {
    	$this->year = $year;
    }

    public function query()
    {
    	return User::select('nim','name','email','telp','tgl_lahir','semester','kelas','jk','alamat','created_at');
    	// return User::query()->whereYear('created_at', $this->year);
    }

    public function headings(): array
    {
    	return [
    		'nim',
    		'nama',
    		'email',
    		'telp',
    		'tgl lahir',
    		'semester',
    		'kelas',
    		'JK',
    		'alamat',
    		'mendaftar pada'
    	];
    }

    public function map($user): array
    {
    	return [
    		// 'namane : '.$user->name,
    		$user->nim,
    		$user->name,
    		$user->email,
    		$user->telp,
    		Date::stringToExcel($user->tgl_lahir),
    		$user->semester,
    		$user->kelas,
    		$user->jk,
    		$user->alamat,
    		Date::dateTimeToExcel($user->created_at),
    	];
    }

    public function columnFormats(): array
    {
    	return [
    		'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    		'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    	];
    }
}
