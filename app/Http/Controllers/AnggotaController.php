<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Hash;
use DB;

class AnggotaController extends Controller
{
    public function index()
    {
    	$anggota = User::all();
    	return view('backend.pages.anggota.index', compact('anggota'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // return $request->all();die;
    	$request->validate([
    		'nim' => 'required|numeric',
    		'nama' => 'required|string',
    		'password' => 'required',
    		'email' => 'required|email',
    		'telp' => 'required|numeric',
    		'tgl_lahir' => 'required|date',
    		'jk' => 'required',
    		'semester' => 'required|numeric',
    		'kelas' => 'required|string',
    		'foto' => 'required|image|max:2048',
    		'alamat' => 'required|string',
    	]);

    	$foto = $request->file('foto');
    	$new_name = date('dmYHis') .'.'. $foto->getClientOriginalExtension();
    	$foto->move(public_path('foto_anggota'), $new_name);

    	$user = new User();
    	$user->nim = $request->nim;
    	$user->name = $request->nama;
    	$user->password = bcrypt($request->password);
    	$user->email = $request->email;
    	$user->telp = $request->telp;
    	$user->tgl_lahir = $request->tgl_lahir;
    	$user->jk = $request->jk;
    	$user->semester = $request->semester;
    	$user->kelas = $request->kelas;
    	$user->foto = $new_name;
    	$user->alamat = $request->alamat;
    	$user->save();

    	return redirect()->route('anggota.index')->with('success', 'Data berhasil ditambahkan');


        // return $request->file('foto')->extension();die;

        // if ($request->file('foto')) {
        //     die('ada foto');
        // }

        // // if ($request->has(['nama','email'])) {
        // if ($request->filled(['nama','email'])) {
        //     die('yeeah nama');
        // }
        // return $request->query();die;

        // if ($request->isMethod('post')) {
        //     die('yeeah');
        // }
    }

    public function show($id)
    {
    	$user = User::findOrFail($id);
    	if (!$user) {
    		return back()->with('warning', 'Anggota tidak ditemukan');
    	}
    	return view('backend.pages.anggota.show', compact('user'));
    }

    public function edit($id)
    {
    	$user = User::findOrFail($id);
    	if (!$user) {
    		return back()->with('warning', 'Anggota tidak ditemukan');
    	}
    	return view('backend.pages.anggota.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        // return $request->all();die;
        // $user = User::findOrFail($id);

    	$request->validate([
    		'nim' => 'required|numeric',
    		'nama' => 'required|string',
    		'password' => 'required',
    		'email' => 'required|email',
    		'telp' => 'required|numeric',
    		'tgl_lahir' => 'required|date',
    		'jk' => 'required',
    		'semester' => 'required|numeric',
    		'kelas' => 'required|string',
    		'new_foto' => 'nullable|image|max:2048',
    		'alamat' => 'required|string',
    	]);

    	if ($request->new_password != '') {
    		$newPassword = bcrypt($request->new_password);
    	}else{
    		$newPassword = $request->password;
    	}

    	if ($request->hasFile('new_foto')) {
    		// $path = public_path().'\foto_anggota';
    		// if (!file_exists($path)) {
    		// 	mkdir($path, 0777, true);
    		// }
    		// unlink($path.$request->foto);
    		$new_foto = $request->file('new_foto');
    		$foto = date('dmYHis') .'.'. $new_foto->getClientOriginalExtension();
    		$new_foto->move(public_path('foto_anggota'), $foto);
    	}else{
    		$foto = $request->foto;
    	}

    	$data = array(
    		'nim' => $request->nim,
    		'name' => $request->nama,
    		'email' => $request->email,
    		'password' => $newPassword,
    		'semester' => $request->semester,
    		'kelas' => $request->kelas,
    		'foto' => $foto,
    		'alamat' => $request->alamat,
    		'tgl_lahir' => $request->tgl_lahir,
    		'jk' => $request->jk,
    		'telp' => $request->telp,
    		'telp' => $request->telp,
    	);

    	User::whereId($id)->update($data);
    	return back()->with('success', 'Data berhasil diupdate');
    }

    public function destroy($id)
    {
    	$user = User::findOrFail($id)->delete();

    	return redirect()->route('anggota.index')->with('success', 'Berhasil hapus data');
    }

    public function uploadFoto($foto){
    	$new_name = date('dmYHis') .'.'. $foto->getClientOriginalExtension();
    	$foto->move(public_path('foto_anggota'), $new_name);

    	return $new_name;
    }

    public function uploadExcel($file){
        $path =  storage_path('app');
        $new_name = 'Anggota-'.date('dmYHis') .'.'. $file->getClientOriginalExtension();
        $file->move($path, $new_name);

        return $new_name;
    }

    public function export() 
    {
        $path = public_path('files');
        if (!file_exists($path)) {
            @mkdir($path, 0777, true);
        }
        Excel::store(new UsersExport(2018), 'anggota'.date('dmYHis').'.xlsx');
        return Excel::download(new UsersExport(2018), 'users.xlsx');
    }

    public function import(Request $request) 
    {
        $file = $request->file('file');
        $new_file = $this->uploadExcel($file);
        $path =  storage_path('app');
        $data = (new UsersImport)->toArray($new_file)[0];
        unlink($path.'/'.$new_file);
        unset($data[0]);
        $anggota = [];
        $count_all_data = 0;
        $count_success_data = 0;
        $count_fail_data = 0;
        foreach ($data as $data) {
            $cek_anggota = User::where('nim',$data[0])->orWhere('email',$data[2])->first();
            if($cek_anggota){
                $count_fail_data++;
            }else{
                $tgl_lahir = empty($data[4]) ? date('Y-m-d') : date('Y-m-d', strtotime($data[4]));
                $anggota[] = [
                    'nim' => $data[0],
                    'name' => $data[1],
                    'email' => $data[2],
                    'password' => bcrypt('himsi'.date('Y')),
                    'telp' => $data[3],
                    'tgl_lahir' => $tgl_lahir,
                    'semester' => $data[5],
                    'kelas' => $data[6],
                    'jk' => $data[7],
                    'alamat' => $data[8],
                    'foto' => '',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $count_success_data++;
            }
            $count_all_data++;
        }

        DB::table('users')->insert($anggota);
        return back()->with('success', $count_success_data.' Data berhasil di import & '.$count_fail_data.' gagal diupload. Dari total '.$count_all_data.' data');
    }
}