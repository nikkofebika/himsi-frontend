<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blogs;

class BlogsController extends Controller
{
    public function index()
    {
        $blogs = Blogs::all();
        return view('backend.pages.blogs.index', compact('blogs'));
    }

    public function create()
    {
        return view('backend.pages.blogs.create');
    }

    public function store(Request $request)
    {
        // return $request->all();die;
        $request->validate([
            'title' => 'required|string',
            'posting_by' => 'required|string',
            'image' => 'nullable|image|max:5000|mimes:jpeg,jpg,png',
            'content' => 'required|string'
        ]);

        if ($request->hasFile('image')) {
            $new_image = $this->uploadFoto($request->file('image'));
        }else{
            $new_image = null;
        }

        $data = [
            'title' => $request->title,
            'posting_by' => $request->posting_by,
            'image' => $new_image,
            'content' => $request->content
        ];

        Blogs::create($data);
        return redirect()->route('blogs.index')->with('success', 'Artikel berhasil di posting');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $blog = Blogs::findOrFail($id);
        return view('backend.pages.blogs.edit', compact('blog'));
    }

    public function update(Request $request, $id)
    {
        // return $request->all();
        $blog = Blogs::findOrFail($id);
        $request->validate([
            'title' => 'required|string',
            'posting_by' => 'required|string',
            'image' => 'nullable|image|max:5000|mimes:jpeg,jpg,png',
            'content' => 'required|string'
        ]);

        if ($request->hasFile('image')) {
            $new_image = $this->uploadFoto($request->file('image'));
        }else{
            $new_image = $blog->image;
        }

        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->image = $new_image;
        $blog->posting_by = $request->posting_by;
        $blog->save();

        return back()->with('success', 'Blog berhasil diupdate');
    }

    public function destroy($id)
    {
        //
    }

    public function uploadFoto($file){
        $path = public_path('background_artikel');
        if (!file_exists($path)) {
            @mkdir($path, 0777, true);
        }
        $foto = date('YmdHis').'.'.$file->getClientOriginalExtension();
        $file->move($path, $foto);
        return $foto;
    }
}
