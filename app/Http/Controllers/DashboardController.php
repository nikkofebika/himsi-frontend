<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedules;
use Auth;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $anggota = DB::table('users')->count();
        $schedules = DB::table('schedules')->count();
        return view('backend.pages.index', compact('anggota','schedules'));
    }

    public function ajax_jk(){
        $statJk = DB::table('users')
        ->select(DB::raw('count(id) as user_count, IF(jk = "L", "Laki-laki", "Perempuan") as jk'))
        ->groupBy('jk')
        ->get()->toArray();
        $data = [["jenis_kelamin", "jumlah"]];
        foreach ($statJk as $v) {
            $data[] = [$v->jk, $v->user_count];
        }
        // return $data;die;
        return response()->json($data);die;
    }

    public function ajax_schedules(){
        $schedules = DB::table('schedules')->select('id', 'name','location','description', 'start_date', 'end_date')->get();
        // $date = date("c",strtotime($schedules->start_date));
        $data = [];
        foreach ($schedules as $s) {
            $data[] = [
                'id' => $s->id,
                'name' => $s->name,
                'location' => $s->location,
                'description' => $s->description,
                'start_bulan' => date('m', strtotime($s->start_date)) - 1,
                'start_tgl' => date('d', strtotime($s->start_date)),
                'end_bulan' => date('m', strtotime($s->end_date)) - 1,
                'end_tgl' => date('d', strtotime($s->end_date)),
            ];
        }
        // return $data;die;
        return response()->json($data);die;
    }

    public function ajax_add_schedule(Request $request){
        // $data = ['nama'=>'nikko', 'usia'=>22, 'hobi'=>'mancing'];
        // return $request->all();die;
        // $request->validate([
        //     'event_name' => 'required|string',
        //     'event_location' => 'required|string',
        //     'event_start_date' => 'required|date',
        //     'event_end_date' => 'required|date',
        // ]);
        $save_event = new Schedules();
        $save_event->name = $request->event_name;
        $save_event->location = $request->event_location;
        $save_event->start_date = date('Y-m-d', strtotime($request->event_start_date));
        $save_event->end_date = date('Y-m-d', strtotime($request->event_end_date));
        $save_event->description = $request->event_description;
        if ($save_event->save()) {
            return response()->json(['success'=>true]);die;
        }else{
            return response()->json(['success'=>false]);die;
        }
        // $date = date("c",strtotime($schedules->start_date));
        // $data = [];
        // foreach ($schedules as $s) {
        //     $data[] = [
        //         'id' => $s->id,
        //         'name' => $s->name,
        //         'location' => $s->location,
        //         'start_bulan' => date('m', strtotime($s->start_date)),
        //         'start_tgl' => date('d', strtotime($s->start_date)),
        //         'end_bulan' => date('m', strtotime($s->end_date)),
        //         'end_tgl' => date('d', strtotime($s->end_date)),
        //     ];
        // }
        // // return $data;die;
        // return response()->json($data);die;
    }

    public function ajax_update_schedule(Request $request,$id = null){
        if ($id == null) {
            return response()->json(['success'=>false, 'messages'=>'Event tidak ditemukan']);die;
        }
        $event = Schedules::findOrFail($id);
        if ($event) {
            $event->name = $request->event_name;
            $event->location = $request->event_location;
            $event->start_date = date('Y-m-d', strtotime($request->event_start_date));
            $event->end_date = date('Y-m-d', strtotime($request->event_end_date));
            $event->description = $request->event_description;
            if ($event->save()) {
                return response()->json(['success'=>true, 'messages'=>'Event berhasil diupdate']);die;
            }else{
                return response()->json(['success'=>false, 'messages'=>'Event gagal diupdate']);die;
            }
        }else{
            return response()->json(['success'=>false, 'messages'=>'Event tidak ditemukan']);die;
        }
    }

    public function ajax_delete_schedule($id = null){
        if ($id == null) {
            return response()->json(['success'=>false, 'messages'=>'Event tidak ditemukan']);die;
        }
        $event = Schedules::findOrFail($id);
        if ($event) {
            $event->delete();
            return response()->json(['success'=>true, 'messages'=>'Event berhasil dihapus']);die;
        }else{
            return response()->json(['success'=>false, 'messages'=>'Event tidak ditemukan']);die;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
