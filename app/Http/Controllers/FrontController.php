<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sliders;

class FrontController extends Controller
{
	public function index(){
		$sliders = Sliders::all();
		return view('frontend.pages.index', compact('sliders'));
	}

	public function join(){
		return view('frontend.pages.join');
	}
}
