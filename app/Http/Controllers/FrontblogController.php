<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blogs;

class FrontblogController extends Controller
{
	public function index(){
		$blogs = Blogs::all();
		return view('frontend.pages.blogs.index', compact('blogs'));
	}

	public function show($id){
		$blog = Blogs::findOrFail($id);
		return view('frontend.pages.blogs.show', compact('blog'));
	}
}
