<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sliders;

class SetfrontController extends Controller
{
    public function index(){
    	return view('backend.pages.settings.frontend.index');
    }

    public function sliders(){
    	$sliders = Sliders::all();
    	// return $sliders;die;
    	return view('backend.pages.settings.frontend.sliders', compact('sliders'));
    }

    public function create_sliders(Request $request){
        // return $request->all();
        $request->validate([
            'title' => 'required|string',
            'description' => 'nullable|string',
            'background' => 'required|string',
            'photo' => 'required|image|max:5000|mimes:jpeg,jpg,png'
        ]);

        $photo = $this->uploadFoto($request->photo);
        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'background' => $request->background,
            'photo' => $photo
        ];

        Sliders::create($data);
        return back()->with('success', 'Slider berhasil disimpan');
    }

    public function delete_sliders(Request $request, $id){
    	die('ook');
    	// return $sliders;die;
    	// return view('backend.pages.settings.frontend.sliders', compact('sliders'));
    }

    public function edit_sliders(Request $request){
        // return $request->file('photo');
        $slider_id = $request->id;
        $slider = Sliders::findOrFail($slider_id);
        $request->validate([
            'title' => 'required|string',
            'description' => 'nullable|string',
            'photo' => 'nullable|image|max:5000|mimes:jpeg,jpg,png'
        ]);
        if ($request->hasFile('photo')) {
            $photo = $this->uploadFoto($request->file('photo'));
        }else{
            $photo = $slider->photo;
        }

        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->photo = $photo;
        $slider->save();
        return back()->with('success', 'Slider berhasil diupdate');
    }

    public function enable_sliders($id, $status){
        // return 'ook';
        $slider = Sliders::findOrFail($id);
        if ($status == '1') {
            $slider->status = '0';
            $slider->save();
            return 'slider off';
        }else{
            $slider->status = '1';
            $slider->save();
            return 'slider on';
        }
        // return $status;
        // return $sliders;die;
        // return view('backend.pages.settings.frontend.sliders', compact('sliders'));
    }

    public function informations(){
        return view('backend.pages.settings.frontend.informations');
    }

    public function uploadFoto($file){
        $path = public_path('img/sliders');
        if (!file_exists($path)) {
            @mkdir($path, 0777, true);
        }
        $filename = date('YmdHis').'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        return $filename;
    }
}
