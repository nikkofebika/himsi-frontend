<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // DB::table('users')->insert(

        // );
        $users = [
            [
                'nim' => '12345678',
                'name' => 'nikko',
                'email' => 'nikko@gmail.com',
                'password' => bcrypt('12345678'),
                'semester' => '5',
                'kelas' => '12.6A.03',
                'foto' => 'nikko.jpg',
                'alamat' => 'Graha Lestari Tangerang',
                'tgl_lahir' => '1997-02-27',
                'jk' => 'L',
                'telp' => '098765432112',
            ],
            [
                'nim' => '12345678',
                'name' => 'aliya',
                'email' => 'aliya@gmail.com',
                'password' => bcrypt('12345678'),
                'semester' => '5',
                'kelas' => '12.6A.03',
                'foto' => 'nikko.jpg',
                'alamat' => 'Graha Lestari Tangerang',
                'tgl_lahir' => '1997-02-27',
                'jk' => 'L',
                'telp' => '098765432112',
            ],
            [
                'nim' => '12345678',
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('12345678'),
                'semester' => '5',
                'kelas' => '12.6A.03',
                'foto' => 'nikko.jpg',
                'alamat' => 'Graha Lestari Tangerang',
                'tgl_lahir' => '1997-02-27',
                'jk' => 'L',
                'telp' => '098765432112',
            ]
        ];

        foreach ($users as $user => $value) {
            User::create($value);
        }
        
    }
}
