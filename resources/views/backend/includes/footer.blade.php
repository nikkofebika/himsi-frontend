<div class="row footer">
	<div class="col-md-6 text-left">
		Copyright © <?php echo date('Y') ?> <a href="{{ route('dashboard') }}">HIMSI CIMONE</a> All rights reserved.
	</div>
	<div class="col-md-6 text-right">
		Design and Developed with <span style="color: red">&#10084;</span>
	</div> 
</div>