<div class="row">
	<div class="col-md-12">

		<!-- Start Quick Menu -->
		<ul class="panel quick-menu clearfix">
			<li class="col-sm-2 <?php echo Request::path() == 'dashboard/settings/sliders' ? 'active':'' ?>">
				<a href="{{ route('settings.sliders') }}"><i class="fa fa-life-ring"></i>Slider</a>
			</li>
			<li class="col-sm-2">
				<a href="#"><i class="fa fa-line-chart"></i>Daily Statistics<span class="label label-danger">12</span></a>
			</li>
			<li class="col-sm-2">
				<a href="#"><i class="fa fa-upload"></i>Upload</a>
			</li>
			<li class="col-sm-2">
				<a href="#"><i class="fa fa-cogs"></i>Settings</a>
			</li>
			<li class="col-sm-2">
				<a href="#"><i class="fa fa-users"></i>Users</a>
			</li>
			<li class="col-sm-2 <?php echo Request::path() == 'dashboard/settings/informations' ? 'active':'' ?>">
				<a href="{{ route('settings.informations') }}"><i class="fa fa-rocket"></i>Information</a>
			</li>
		</ul>
		<!-- End Quick Menu -->

	</div>
</div>