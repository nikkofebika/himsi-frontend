<div class="sidebar clearfix">

		<ul class="sidebar-panel nav">
			<li class="sidetitle">MAIN</li>
			<li><a href="{{ route('dashboard') }}"><span class="icon color5"><i class="fa fa-home"></i></span>Dashboard</a></li>
			<li><a href="{{ route('anggota.index') }}"><span class="icon color6"><i class="fa fa-users"></i></span>Anggota<span class="label label-default">19</span></a></li>
			<li><a href="{{ route('blogs.index') }}"><span class="icon color6"><i class="fa fa-newspaper-o"></i></span>Blogs<span class="label label-default">19</span></a></li>
			<li><a href="#"><span class="icon color7"><i class="fa fa-cog"></i></span>Settings<span class="caret"></span></a>
				<ul>
					<li><a href="{{ route('settings.sliders') }}">Frontend</a></li>
					<li><a href="#">Backend</a></li>
					<li><a href="#">Our Location</a></li>
				</ul>
			</li>
		</ul>

		<div class="sidebar-plan">
			Pro Plan<a href="#" class="link">Upgrade</a>
			<div class="progress">
				<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
				</div>
			</div>
			<span class="space">42 GB / 100 GB</span>
		</div>

	</div>