<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
	<meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
	<title>Kode - Premium Bootstrap Admin Template</title>

	<!-- ========== Css Files ========== -->
	<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900,700italic,500italic,400italic,300italic,100italic" rel="stylesheet">
	<link href="{{ asset('backend/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/shortcuts.css')  }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')  }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/bootstrap-select/bootstrap-select.css')  }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/bootstrap-toggle/bootstrap-toggle.min.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/summernote/summernote.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/summernote/summernote-bs3.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/sweet-alert/sweet-alert.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/datatables/datatables.css') }}" rel="stylesheet">
	<!-- <link href="{{ asset('backend/css/plugin/chartist/chartist.min.css') }}" rel="stylesheet"> -->
	<link href="{{ asset('backend/css/plugin/rickshaw/rickshaw.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/rickshaw/detail.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/rickshaw/graph.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/rickshaw/legend.css') }}" rel="stylesheet">
	<link href="{{ asset('backend/css/plugin/date-range-picker/daterangepicker-bs3.css') }}" rel="stylesheet">
	<!-- <link href="{{ asset('backend/css/plugin/fullcalendar/fullcalendar.css') }}" rel="stylesheet"> -->
	@yield('includeCss')

</head>
<body>
	<!-- Start Page Loading -->
	<div class="loading"><img src="{{ asset('backend/img/loading.gif') }}" alt="loading-img"></div>
	<!-- End Page Loading -->

	<!-- START TOP -->
	@include('backend.includes.header')
	<!-- END TOP -->

	<!-- START SIDEBAR -->
	@include('backend.includes.sidebar')
	<!-- END SIDEBAR -->

	<!-- START CONTENT -->
	@yield('content')
	<!-- End Content -->

	<!-- START SIDEPANEL -->
	@include('backend.includes.sidepanel')
	<!-- END SIDEPANEL -->

	@yield('modal')
<script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrap-select/bootstrap-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/summernote/summernote.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('backend/js/flot-chart/flot-chart.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/flot-chart/flot-chart-time.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/flot-chart/flot-chart-stack.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/flot-chart/flot-chart-pie.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/flot-chart/flot-chart-plugin.js') }}"></script> -->
<!-- <script type="text/javascript" src="{{ asset('backend/js/chartist/chartist.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/chartist/chartist-plugin.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('backend/js/easypiechart/easypiechart.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/easypiechart/easypiechart-plugin.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/sparkline/sparkline.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/sparkline/sparkline-plugin.js') }}"></script>
<!-- <script src="{{ asset('backend/js/rickshaw/d3.v3.js') }}"></script> -->
<!-- <script src="{{ asset('backend/js/rickshaw/rickshaw.js') }}"></script> -->
<!-- <script src="{{ asset('backend/js/rickshaw/rickshaw-plugin.js') }}"></script> -->
<script src="{{ asset('backend/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('backend/js/sweet-alert/sweet-alert.min.js') }}"></script>
<script src="{{ asset('backend/js/kode-alert/main.js') }}"></script>
<!-- <script src="{{ asset('backend/http://maps.google.com/maps/api/js?sensor=true') }}"></script> -->
<!-- <script src="{{ asset('backend/js/gmaps/gmaps.js') }}"></script> -->
<!-- <script src="{{ asset('backend/js/gmaps/gmaps-plugin.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('backend/js/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/moment/moment.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('backend/js/full-calendar/fullcalendar.js') }}"></script> -->
<!-- <script type="text/javascript" src="{{ asset('backend/js/date-range-picker/daterangepicker.js') }}"></script> -->

<script>

// set up our data series with 50 random data points

// var seriesData = [ [], [], [] ];
// var random = new Rickshaw.Fixtures.RandomData(20);

// for (var i = 0; i < 110; i++) {
// 	random.addData(seriesData);
// }

// // instantiate our graph!

// var graph = new Rickshaw.Graph( {
// 	element: document.getElementById("todaysales"),
// 	renderer: 'bar',
// 	series: [
// 	{
// 		color: "#33577B",
// 		data: seriesData[0],
// 		name: 'Photodune'
// 	}, {
// 		color: "#77BBFF",
// 		data: seriesData[1],
// 		name: 'Themeforest'
// 	}, {
// 		color: "#C1E0FF",
// 		data: seriesData[2],
// 		name: 'Codecanyon'
// 	}
// 	]
// } );

// graph.render();

// var hoverDetail = new Rickshaw.Graph.HoverDetail( {
// 	graph: graph,
// 	formatter: function(series, x, y) {
// 		var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
// 		var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
// 		var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
// 		return content;
// 	}
// } );

// </script>

// <!-- Today Activity -->
// <script>
// // set up our data series with 50 random data points

// var seriesData = [ [], [], [] ];
// var random = new Rickshaw.Fixtures.RandomData(20);

// for (var i = 0; i < 50; i++) {
// 	random.addData(seriesData);
// }

// // instantiate our graph!

// var graph = new Rickshaw.Graph( {
// 	element: document.getElementById("todayactivity"),
// 	renderer: 'area',
// 	series: [
// 	{
// 		color: "#9A80B9",
// 		data: seriesData[0],
// 		name: 'London'
// 	}, {
// 		color: "#CDC0DC",
// 		data: seriesData[1],
// 		name: 'Tokyo'
// 	}
// 	]
// } );

// graph.render();

// var hoverDetail = new Rickshaw.Graph.HoverDetail( {
// 	graph: graph,
// 	formatter: function(series, x, y) {
// 		var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
// 		var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
// 		var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
// 		return content;
// 	}
// } );
</script>
@yield('includeJs')
</body>
</html>