@extends('backend.layouts.master')

@section('content')
<div class="content">

	<div class="page-header">
		<h1 class="title">Anggota</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('dashboard') }}">Dashboard</a></li>
			<li class="active">Anggota</li>
		</ol>

		<div class="right">
			<div class="btn-group" role="group" aria-label="...">
				<a href="index.html" class="btn btn-light">Dashboard</a>
				<a href="#" class="btn btn-light"><i class="fa fa-refresh"></i></a>
				<a href="#" class="btn btn-light"><i class="fa fa-search"></i></a>
				<a href="#" class="btn btn-light" id="topstats"><i class="fa fa-line-chart"></i></a>
			</div>
		</div>

	</div>
	<div class="container-padding">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<form class="fieldset-form" method="POST" action="{{ route('anggota.update', $user->id) }}" enctype="multipart/form-data">
						@csrf
						@method('PATCH')
						<fieldset>
							<legend>Edit Biodata</legend>
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							@if (Session::has('success'))
							<div class="kode-alert kode-alert-icon alert3">
								<i class="fa fa-check"></i>
								<a href="#" class="closed">&times;</a>
								{{ Session::get('success') }}
							</div>
							@endif
							<div class="form-group  col-md-6">
								<label class="form-label">NIM</label>
								<input name="nim" type="text" class="form-control" value="{{ $user->nim }}">
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Nama</label>
								<input name="nama" type="text" class="form-control" value="{{ $user->name }}">
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Password</label>
								<input type="password" name="new_password" class="form-control" value="">
								<input type="hidden" name="password" class="form-control" value="{{ $user->password }}">
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Email</label>
								<input type="text" name="email" class="form-control" value="{{ $user->email }}">
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">No. Telp / WA</label>
								<input type="telp" name="telp" class="form-control" value="{{ $user->telp }}">
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Tanggal Lahir</label>
								<input type="date" name="tgl_lahir" class="form-control" value="{{ $user->tgl_lahir }}">
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Jenis Kelamin</label>
								<select name="jk" class="form-control">
									<option value="L">Laki - laki</option>
									<option value="P">Perempuan</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Semester</label>
								<select name="semester" class="form-control">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label class="form-label">Kelas</label>
								<input type="text" name="kelas" class="form-control" value="{{ $user->kelas }}">
							</div>
							<div class="form-group col-md-6">
								<img src="{{ asset('foto_anggota/'.$user->foto) }}" width="100px">
								<label class="form-label">Foto</label>
								<input type="file" name="new_foto" class="form-control">
								<input type="hidden" name="foto" class="form-control" value="{{ $user->foto }}">
							</div>
							<div class="form-group col-md-12">
								<label class="form-label">Alamat</label>
								<textarea class="form-control" name="alamat" rows="3">{{ $user->alamat }}</textarea>
							</div>
							<center><button type="submit" class="btn btn-default">Submit</button></center>
						</fieldset>
					</form>

				</div>

			</div>
		</div>
	</div>
	<!-- Start Footer -->
	@include('backend.includes.footer')
	<!-- End Footer -->


</div>
@endsection