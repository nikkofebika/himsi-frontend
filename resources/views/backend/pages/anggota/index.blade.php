@extends('backend.layouts.master')

@section('content')
<div class="content">

	<!-- Start Page Header -->
	<div class="page-header">
		<h1 class="title">Anggota</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('dashboard') }}">Dashboard</a></li>
			<li class="active">Anggota</li>
		</ol>

		<!-- Start Page Header Right Div -->
		<div class="right">
			<div class="btn-group" role="group" aria-label="...">
				<a href="#" class="btn btn-light" data-toggle="modal" data-target="#addAnggota"><span class="fa fa-plus"></span> Tambah Anggota</a>
				<a href="{{ route('anggota.export') }}" class="btn btn-light"><i class="fa fa-file-excel-o"></i> export</a>
				<a href="#" class="btn btn-light" data-toggle="modal" data-target="#importAnggota"><span class="fa fa-plus"></span> Import Anggota</a>
			</div>
		</div>
		<!-- End Page Header Right Div -->

	</div>
	<!-- End Page Header -->

	<div class="container-padding">

		<div class="row">

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-title">
						Data Anggota
					</div>
					@include('flash_message')
					<div class="panel-body table-responsive">

						<table id="example0" class="table display">
							<thead>
								<tr>
									<th>NIM</th>
									<th>Nama</th>
									<th>Email</th>
									<th>Semester</th>
									<th>Kelas</th>
									<th>Opsi</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($anggota as $user)
								<tr>
									<td>{{ $user->nim }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->semester }}</td>
									<td>{{ $user->kelas }}</td>
									<td>
										<a href="{{ url('anggota/'.$user->id.'/edit') }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
										<a href="{{ url('anggota/'.$user->id) }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
										<form action="{{ route('anggota.destroy', $user->id) }}" method="POST">
											@method('DELETE')
											@csrf
											<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Anda yakin akan menghapus data ini ?')"><i class="fa fa-trash"></i></button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>

				</div>
			</div>
			<!-- End Panel -->
		</div>
		<!-- End Row -->
	</div>
	<!-- Start Footer -->
	<@include('backend.includes.footer')
	<!-- End Footer -->


</div>
@endsection

@section('modal')
<div class="modal fade" id="importAnggota" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Import Data Anggota Baru</h4>
			</div>
			<div class="modal-body">
				<div class="kode-alert kode-alert-icon alert5">
					<div>
						<i class="fa fa-warning"></i> PENTING
					</div>
					<p>Untuk dapat menggunakan fitur import data anggota, Kami sudah menyiapkan template Ms. Excel untuk di download. Kami sangat menyarankan untuk menggunakan template ini, Template ini kami sediakan agar tidak terjadi kesalahan input data pada proses import nanti. <a href="#" title="Download Template" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> Download template disini</a></p>
					<p>Sebelum mengimport perhatikan :</p>
					<ol>
						<li>NIM dan Email mahasiswa tidak boleh sama</li>
						<li>Semua kolom yang ada di template wajib diisi agar tidak terjadi error pada saat mengimport data</li>
						<li>Untuk kolom tanggal lahir. Format penulisannya adalah, contoh : 27-02-1997</li>
						<li>Jenis kelamin harap diisi L/P saja</li>
						<li>Password defaut setiap anggota adalah "himsi2019". Password ini digunakan untuk login anggota</li>
						<li>Mohon untuk mengikuti semua peraturan ini. Kalo error saya ga tanggung jawab wkwk</li>
					</ol>
				</div>
				<form method="POST" action="{{ route('anggota.import') }}" enctype="multipart/form-data">
					@csrf
					<input type="file" name="file" required class="form-control">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-default">Import</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Tambah Anggota -->
<div class="modal fade" id="addAnggota" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Data Anggota Baru</h4>
			</div>
			<form method="POST" action="{{ route('anggota.store') }}" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="form-group">
								<label class="form-label">NIM</label>
								<input type="number" name="nim" maxlength="8" class="form-control form-control-line">
							</div>
							<div class="form-group">
								<label class="form-label">Nama lengkap</label>
								<input type="text" name="nama" class="form-control form-control-line">
							</div>
							<div class="form-group">
								<label class="form-label">Password</label>
								<input type="password" name="password" class="form-control form-control-line">
							</div>
							<div class="form-group">
								<label class="form-label">Email</label>
								<input type="email" name="email" class="form-control form-control-line">
							</div>
							<div class="form-group">
								<label class="form-label">No. Telp / WA</label>
								<input type="number" name="telp" class="form-control form-control-line">
							</div>
							<div class="form-group">
								<label class="form-label">Tanggal Lahir</label>
								<input type="date" name="tgl_lahir" class="form-control form-control-line">
							</div>
							<div class="form-group">
								<label class="form-label">Jenis Kelamin</label>
								<select name="jk" class="form-control">
									<option value="L">Laki - laki</option>
									<option value="P">Perempuan</option>
								</select>
							</div>
							<div class="form-group">
								<label class="form-label">Semester</label>
								<select name="semester" class="form-control">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
								</select>
							</div>
							<div class="form-group">
								<label class="form-label">Kelas</label>
								<input type="text" maxlength="8" name="kelas" class="form-control form-control-line">
							</div>
							<div class="form-group">
								<label class="form-label">Foto</label>
								<input type="file" name="foto" class="form-control form-control-line" required>
							</div>
							<div class="form-group">
								<label class="form-label">Alamat</label>
								<textarea class="form-control form-control-line" name="alamat" rows="3"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-default">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('includeJs')
<script>
	$(document).ready(function() {
		$('#example0').DataTable();
	} );
</script>



<script>
	$(document).ready(function() {
		var table = $('#example').DataTable({
			"columnDefs": [
			{ "visible": false, "targets": 2 }
			],
			"order": [[ 2, 'asc' ]],
			"displayLength": 25,
			"drawCallback": function ( settings ) {
				var api = this.api();
				var rows = api.rows( {page:'current'} ).nodes();
				var last=null;

				api.column(2, {page:'current'} ).data().each( function ( group, i ) {
					if ( last !== group ) {
						$(rows).eq( i ).before(
							'<tr class="group"><td colspan="5">'+group+'</td></tr>'
							);

						last = group;
					}
				} );
			}
		} );

		$('#example tbody').on( 'click', 'tr.group', function () {
			var currentOrder = table.order()[0];
			if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
				table.order( [ 2, 'desc' ] ).draw();
			}
			else {
				table.order( [ 2, 'asc' ] ).draw();
			}
		} );
	} );
</script>
@endsection