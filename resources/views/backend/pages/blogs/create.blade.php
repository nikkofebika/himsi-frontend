@extends('backend.layouts.master')

@section('content')
<div class="content">
	<div class="page-header">
		<h1 class="title">Blog / Artikel</h1>
		<ol class="breadcrumb">
			<li><a href="/">Dashboard</a></li>
			<li><a href="{{ route('blogs.index') }}">Blog</a></li>
			<li class="active">Create</li>
		</ol>
		<div class="right">
			<a href="{{ URL::previous() }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
		</div>
	</div>
	<!-- START CONTAINER -->
	<div class="container-padding">

		<!-- Start Row -->
		<div class="row">


			<div class="col-md-12">
				<div class="panel panel-default">

					<div class="panel-title">
						Berkreasilah Sesukamu :)
						@if(Session::has('success'))
						<div class="alert alert-success">
							{{ Session::get('success') }}
						</div>
						@endif
						@if($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach($errors->all() as $error)
								{{ $error }}
								@endforeach
							</ul>
						</div>
						@endif
					</div>

					<div class="panel-body">

						<form method="POST" action="{{ route('blogs.store') }}" enctype="multipart/form-data">
							@csrf
							<div class="form-group">
								<label class="form-label">Judul</label>
								<input name="title" type="text" class="form-control" required>
							</div>
							<div class="form-group">
								<label class="form-label">Posting By</label>
								<input type="text" name="posting_by" class="form-control" placeholder="Himsi Cimone" value="Himsi Cimone" required>
							</div>
							<div class="form-group">
								<label class="form-label">Background</label>
								<input name="image" type="file" class="form-control">
							</div>
							<label class="form-label">Konten</label>
							<textarea name="content" class="summernote"></textarea>
							<button class="btn btn-default btn-block"><i class="fa fa-paper-plane-o"></i> Posting</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Start Footer -->
	@include('backend.includes.footer')
	<!-- End Footer -->
</div>
@endsection

@section('includeJs')
<script>

	/* SUMMERNOTE*/
	$(document).ready(function() {
		$('.summernote').summernote();
	});
</script>
@endsection