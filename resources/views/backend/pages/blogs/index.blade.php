@extends('backend.layouts.master')

@section('content')
<div class="content">

	<!-- Start Page Header -->
	<div class="page-header">
		<h1 class="title">Blogs / Artikel</h1>
		<ol class="breadcrumb">
			<li><a href="/">Dashboard</a></li>
			<li class="active">Blogs</li>
		</ol>

		<!-- Start Page Header Right Div -->
		<div class="right">
			<!-- <div class="btn-group" role="group" aria-label="..."> -->
				<a href="{{ route('blogs.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Buat Blog</a>
				<!-- </div> -->
			</div>
			<!-- End Page Header Right Div -->

		</div>
		<!-- End Page Header -->

		<!-- START CONTAINER -->
		<div class="container-padding">

			<!-- Start Row -->
			<div class="row">


				<div class="col-md-12">
					<div class="panel panel-default">

						<div class="panel-title">
							Our Blogs
							@include('flash_message')
						</div>

						<div class="panel-body">

							<!-- Start BlogPost -->
							@if(count($blogs) == 0)
							<div class="kode-alert kode-alert-icon alert5">
								<center><i class="fa fa-warning"></i>Belum ada blog yang dibuat. Silahkan buat dahulu :)</center>
							</div>
							@else
							@foreach($blogs as $blog)
							<div class="col-md-4">
								<div class="panel panel-widget blog-post">
									<div class="panel-body">

										<div class="image-div color10-bg">
											<img src="{{ asset('background_artikel/'.$blog->image) }}" class="image" alt="img">
											<h1 class="title"><a href="#">{{ $blog->title }}</a></h1>
										</div>
										<p class="text">{{ substr($blog->content, 0, 100) }}...</p>
										<div>
											<a href="#" sty>Read More</a> <a href="{{ url('blogs/'.$blog->id.'/edit') }}" class="btn btn-primary btn-xs pull-right"><i class="fa fa-pencil"></i> Edit</a>
										</div>
										<hr style="margin: 0;">
										<div><strong>By : </strong>{{ $blog->posting_by }}</div>
										<div><strong>Created At : </strong>{{ $blog->created_at }}</div>
										<div><strong>View : </strong>{{ $blog->view }} <i class="fa fa-eye"></i></div>
										<div><strong>Like : </strong>{{ $blog->like }} <i class="fa fa-thumbs-up"></i></div>
										<div><strong>Tags : </strong>{{ $blog->tags }}</div>
									</div>
								</div>
							</div>
							@endforeach
							@endif
							<!-- End BlogPost -->      
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Start Footer -->
		@include('backend.includes.footer')
		<!-- End Footer -->
	</div>
	@endsection

	@section('includeJs')
	<script>

		/* SUMMERNOTE*/
		$(document).ready(function() {
			$('.summernote').summernote();
		});
	</script>
	@endsection