@extends('backend.layouts.master')

@section('content')
<div class="content">

	<!-- Start Page Header -->
	<div class="page-header">
		<h1 class="title">Setting Frontend</h1>
		<ol class="breadcrumb">
			<li><a href="/">Dashboard</a></li>
			<li><a href="#">Settings</a></li>
			<li class="active">Frontend</li>
		</ol>

		<!-- Start Page Header Right Div -->
		<div class="right">
			<!-- <div class="btn-group" role="group" aria-label="..."> -->
				<a href="{{ route('blogs.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Buat Blog</a>
				<!-- </div> -->
			</div>
			<!-- End Page Header Right Div -->

		</div>
		<!-- End Page Header -->

		<!-- START CONTAINER -->
		<div class="container-padding">

			<!-- Start Row -->
			<div class="row">


				<div class="col-md-12">
					<div class="panel panel-default">

						<div class="panel-title">
							Our Blogs
							@if(Session::has('success'))
							<div class="alert alert-success">
								{{ Session::get('success') }}
							</div>
							@endif
							@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach($errors->all() as $error)
									{{ $error }}
									@endforeach
								</ul>
							</div>
							@endif
						</div>

						<div class="panel-body">
							<!-- Start Row -->
							<div class="row">
								<div class="col-md-12">

									<!-- Start Quick Menu -->
									<ul class="panel quick-menu clearfix">
										<li class="col-sm-2">
											<a href="{{ route('settings.sliders') }}"><i class="fa fa-life-ring"></i>Slider</a>
										</li>
										<li class="col-sm-2">
											<a href="#"><i class="fa fa-line-chart"></i>Daily Statistics<span class="label label-danger">12</span></a>
										</li>
										<li class="col-sm-2">
											<a href="#"><i class="fa fa-upload"></i>Upload</a>
										</li>
										<li class="col-sm-2">
											<a href="#"><i class="fa fa-cogs"></i>Settings</a>
										</li>
										<li class="col-sm-2">
											<a href="#"><i class="fa fa-users"></i>Users</a>
										</li>
										<li class="col-sm-2">
											<a href="#"><i class="fa fa-rocket"></i>Information</a>
										</li>
									</ul>
									<!-- End Quick Menu -->

								</div>
							</div>
							<!-- End Row -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Start Footer -->
		@include('backend.includes.footer')
		<!-- End Footer -->
	</div>
	@endsection