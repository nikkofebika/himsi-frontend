@extends('backend.layouts.master')

@section('content')
<style type="text/css">
	.active{
		background: #D3D3D3;
	}
</style>
<div class="content">

	<!-- Start Page Header -->
	<div class="page-header">
		<h1 class="title">Setting Frontend</h1>
		<ol class="breadcrumb">
			<li><a href="/">Dashboard</a></li>
			<li><a href="#">Settings</a></li>
			<li class="active">Frontend</li>
		</ol>

		<!-- START CONTAINER -->
		<div class="container-padding">

			<!-- Start Row -->
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">

						<div class="panel-body">
							<!-- Start Row -->
							@include('backend.includes.setting_frontend.quick_menu')
							<hr>
							<!-- End Row -->
							@include('flash_message')
							<a href="#" data-toggle="modal" data-target="#mdl_create_slider" class="btn btn-primary mb-5"><i class="fa fa-plus"></i> Buat Slider</a>
							<div class="table-responsive">
								<table class="table table-hover table-striped table-bordered">
									<thead>
										<tr>
											<th>No</th>
											<th>Title</th>
											<th>Description</th>
											<th>Photo</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1 ?>
										@foreach($sliders as $slider)
										<tr>
											<td>{{ $no++ }}</td>
											<td>{{ $slider->title }}</td>
											<td>{{ $slider->description }}</td>
											<td><a target="_blank" href="{{ asset('frontend/images/sliders/'.$slider->photo) }}" title=""><img width="30px" src="{{ asset('frontend/images/sliders/'.$slider->photo) }}" alt=""></a></td>
											<td>
												<input type="checkbox" <?php echo $slider->status == '1' ? 'checked' : '' ?> class="toggle_status" data-id_slider="{{ $slider->id }}" data-status_slider="{{ $slider->status }}" data-toggle="toggle" data-on="ON" data-off="OFF" data-onstyle="success" data-offstyle="danger">
											</td>
											<td>
												<a href="#" title="" data-toggle="modal" data-target="#mdl_edit_slider" class="btn btn-warning btn-xs" data-id="{{ $slider->id }}" data-title="{{ $slider->title }}" data-desc="{{ $slider->description }}"><i class="fa fa-pencil"></i></a>
												<form action="{{ route('settings.sliders.delete', $slider->id) }}" method="POST">
													@csrf
													@method('DELETE')
													<button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
												</form>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Start Footer -->
		@include('backend.includes.footer')
		<!-- End Footer -->
	</div>
	@endsection

	@section('modal')
	<div class="modal fade" id="mdl_create_slider" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Buat SLider</h4>
				</div>
				<form action="{{ route('settings.create') }}" method="POST" enctype="multipart/form-data">
					<div class="modal-body">
						@csrf
						<div class="form-group">
							<label>Title</label>
							<input type="text" name="title" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Description</label>
							<textarea name="description" class="form-control" rows="3"></textarea>
						</div>
						<div class="form-group">
							<label>Background</label>
							<select name="background" class="form-control" required>
								<option value="yellow">Yellow</option>
								<option value="green">Green</option>
								<option value="blue">Blue</option>
							</select>
						</div>
						<div class="form-group">
							<label>Gambar (<small class="color9">Ubah gambar</small>)</label>
							<input type="file" name="photo" class="form-control" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-default">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="mdl_edit_slider" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Edit SLider</h4>
				</div>
				<form action="{{ route('sliders.edit') }}" method="POST" enctype="multipart/form-data">
					<div class="modal-body">
						@csrf
						<input type="hidden" name="id" id="view-id">
						<div class="form-group">
							<label>Title</label>
							<input type="text" name="title" class="form-control" required id="view-title">
						</div>
						<div class="form-group">
							<label>Description</label>
							<textarea name="description" class="form-control" rows="3" id="view-desc"></textarea>
						</div>
						<div class="form-group">
							<label>Gambar (<small class="color9">Ubah gambar</small>)</label>
							<input type="file" name="photo" class="form-control">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-default">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endsection

	@section('includeJs')
	<script type="text/javascript">
		$('#mdl_edit_slider').on('show.bs.modal', function(e){
			var button = $(e.relatedTarget);
			console.log(button);
			console.log(button.data('id'));
			$('#view-id').val(button.data('id'));
			$('#view-title').val(button.data('title'));
			$('#view-desc').val(button.data('desc'));
		});

		// JS ON OFF SLIDERS
		$('.toggle_status').on('change',function () {
			var id = $(this).data('id_slider');
			var status = $(this).data('status_slider');
			console.log(id)
			console.log(status)
			if ($(this).is(':checked')) {
				$.get("{{ url('/dashboard/sliders/enable') }}/"+id+"/"+status, function(res){
					// $(this).data('status_slider',0);
					// console.log(res)
					location.reload(false) 
				},'json');
			} else {
				$.get("{{ url('/dashboard/sliders/enable') }}/"+id+"/"+status, function(res){
					// $(this).data('status_slider',1);
					// console.log(res)
					location.reload(false) 
				},'json');
			}
		});
	</script>
	@endsection