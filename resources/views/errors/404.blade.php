@extends('frontend.layouts.errors')

@section('content')
<div class="page_content_wrap">
	<div class="content_wrap">
		<div class="content">
			<article class="post_item post_item_404">
				<div class="post_content">
					<h1 class="page_title">404</h1>
					<h2 class="page_subtitle">The requested page cannot be found</h2>
					<p class="page_description">Can't find what you need? Take a moment and do a search below or start from <a href="/">our homepage</a>.</p>
				</div>
			</article>
		</div>
	</div>
</div>
@endsection