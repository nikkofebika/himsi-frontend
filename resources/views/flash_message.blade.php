@if(Session::has('success'))
<div class="kode-alert kode-alert-icon alert3">
	<i class="fa fa-check"></i>
	<a href="#" class="closed">×</a>
	{{ Session::get('success') }}
</div>
@elseif(Session::has('warning'))
<div class="kode-alert kode-alert-icon alert5">
	<i class="fa fa-warning"></i>
	<a href="#" class="closed">×</a>
	{{ Session::get('warning') }}
</div>
@elseif(Session::has('info'))
<div class="kode-alert kode-alert-icon alert4">
	<i class="fa fa-info"></i>
	<a href="#" class="closed">×</a>
	{{ Session::get('info') }}
</div>
@elseif(Session::has('error'))
<div class="kode-alert kode-alert-icon alert6">
	<i class="fa fa-lock"></i>
	<a href="#" class="closed">×</a>
	{{ Session::get('error') }}
</div>
@elseif($errors->any())
<div class="kode-alert kode-alert-icon alert6">
	<a href="#" class="closed">×</a>
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif