<!-- Widgets Footer -->
<footer class="footer_wrap bg_tint_light footer_style_white widget_area">
	<div class="content_wrap">
		<div class="columns_wrap">
			<!-- Calendar widget -->
			<aside class="column-1_3 widget widget_calendar">
				<h5 class="widget_title">Calendar</h5>
				<table>
					<thead>
						<tr>
							<th class="month_prev">
								<a href="#" data-type="post,courses,tribe_events" data-year="2015" data-month="01" title="View posts for January 2015"></a>
							</th>
							<th class="month_cur" colspan="5">September <span>2015</span></th>
							<th class="month_next"> 
								<a href="#" data-month="10" data-year="2015" data-type="post,courses,tribe_events" title="View posts for October 2015"></a>
							</th>
						</tr>
						<tr>
							<th class="weekday" scope="col" title="Monday">Mon</th>
							<th class="weekday" scope="col" title="Tuesday">Tue</th>
							<th class="weekday" scope="col" title="Wednesday">Wed</th>
							<th class="weekday" scope="col" title="Thursday">Thu</th>
							<th class="weekday" scope="col" title="Friday">Fri</th>
							<th class="weekday" scope="col" title="Saturday">Sat</th>
							<th class="weekday" scope="col" title="Sunday">Sun</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="1" class="pad"><span class="day_wrap">&nbsp;</span></td>
							<td class="day"><span class="day_wrap">1</span></td>
							<td class="day"><span class="day_wrap">2</span></td>
							<td class="day"><span class="day_wrap">3</span></td>
							<td class="day"><span class="day_wrap">4</span></td>
							<td class="day"><span class="day_wrap">5</span></td>
							<td class="day"><span class="day_wrap">6</span></td>
						</tr>
						<tr>
							<td class="day"><span class="day_wrap">7</span></td>
							<td class="day"><span class="day_wrap">8</span></td>
							<td class="day"><span class="day_wrap">9</span></td>
							<td class="day"><a class="day_wrap" title="Post" href="#">10</a></td>
							<td class="day"><span class="day_wrap">11</span></td>
							<td class="day"><span class="day_wrap">12</span></td>
							<td class="day"><span class="day_wrap">13</span></td>
						</tr>
						<tr>
							<td class="day"><span class="day_wrap">14</span></td>
							<td class="day"><span class="day_wrap">15</span></td>
							<td class="day"><span class="day_wrap">16</span></td>
							<td class="day"><span class="day_wrap">17</span></td>
							<td class="day"><a class="day_wrap" title="Post" href="#">18</a></td>
							<td class="day"><span class="day_wrap">19</span></td>
							<td class="day"><span class="day_wrap">20</span></td>
						</tr>
						<tr>
							<td class="today"><span class="day_wrap">21</span></td>
							<td class="day"><span class="day_wrap">22</span></td>
							<td class="day"><span class="day_wrap">23</span></td>
							<td class="day"><span class="day_wrap">24</span></td>
							<td class="day"><span class="day_wrap">25</span></td>
							<td class="day"><span class="day_wrap">26</span></td>
							<td class="day"><span class="day_wrap">27</span></td>
						</tr>
						<tr>
							<td class="day"><span class="day_wrap">28</span></td>
							<td class="day"><span class="day_wrap">29</span></td>
							<td class="day"><span class="day_wrap">30</span></td>
							<td class="pad" colspan="4"><span class="day_wrap">&nbsp;</span></td>
						</tr>
					</tbody>
				</table>
			</aside>
			<!-- /Calendar widget -->
			<!-- Popular courses widget -->
			<aside class="column-1_3 widget">
				<h5 class="widget_title">Popular Courses</h5>
				<article class="post_item with_thumb first">
					<div class="post_thumb">
						<img alt="" src="http://placehold.it/75x75"></div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="paid-course.html">Principles of Written English, Part 1</a>
							</h6>
							<div class="post_info"><span class="post_info_item post_info_posted">
								<a href="paid-course.html" class="post_info_date">December 24, 2014</a></span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="paid-course.html" class="post_counters_item post_counters_rating icon-star-1">
										<span class="post_counters_number">86.8</span>
									</a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item with_thumb">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="paid-course.html">Medical Chemistry: The  Molecular Basis...</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="paid-course.html" class="post_info_date">March 8, 2015</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="paid-course.html" class="post_counters_item post_counters_rating icon-star-1">
										<span class="post_counters_number">83.8</span>
									</a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item with_thumb">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="paid-course.html">Entrepreneurship 101:  Who is your customer?</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="paid-course.html" class="post_info_date">February 27, 2015</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="paid-course.html" class="post_counters_item post_counters_rating icon-star-1">
										<span class="post_counters_number">76.3</span>
									</a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item with_thumb">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="paid-course.html">Introduction to Biomedical Imaging</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="paid-course.html" class="post_info_date">January 1, 2016</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="paid-course.html" class="post_counters_item post_counters_rating icon-star-1">
										<span class="post_counters_number">74.8</span>
									</a>
								</span>
							</div>
						</div>
					</article>
				</aside>
				<!-- /Popular courses widget -->
				<!-- Recent courses widget -->
				<aside class="column-1_3 widget">
					<h5 class="widget_title">Recent Courses</h5>
					<article class="post_item with_thumb first">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="paid-course.html">Principles of Written English, Part 2</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="paid-course.html" class="post_info_date">February 10, 2015</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="paid-course.html" class="post_counters_item post_counters_views icon-eye">
										<span class="post_counters_number">749</span>
									</a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item with_thumb">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="paid-course.html">Entrepreneurship 101:  Who is your customer?</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="paid-course.html" class="post_info_date">February 27, 2015</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="paid-course.html" class="post_counters_item post_counters_views icon-eye">
										<span class="post_counters_number">729</span>
									</a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item with_thumb">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="free-course.html">Evaluating Social  Programs</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="free-course.html" class="post_info_date">January 1, 2015</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="free-course.html" class="post_counters_item post_counters_views icon-eye">
										<span class="post_counters_number">1154</span>
									</a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item with_thumb">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="paid-course.html">Principles of Written English, Part 1</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="paid-course.html" class="post_info_date">December 24, 2014</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="paid-course.html" class="post_counters_item post_counters_views icon-eye">
										<span class="post_counters_number">253</span>
									</a>
								</span>
							</div>
						</div>
					</article>
				</aside>
				<!-- /Recent courses widget -->
			</div>
		</div>
	</footer>
	<!-- /Widgets Footer -->
	<!-- Testimonials footer -->
	<footer class="testimonials_wrap sc_section bg_tint_dark post_ts_bg3">
		<div class="sc_section_overlay" data-bg_color="#1eaace" data-overlay="0">
			<div class="content_wrap">
				<!-- Testimonials section -->
				<div class="sc_testimonials sc_slider_swiper swiper-slider-container sc_slider_nopagination sc_slider_controls sc_slider_height_fixed aligncenter height_12em width_100per" data-old-height="12em" data-interval="7000">
					<div class="slides swiper-wrapper">
						<div class="swiper-slide height_12em width_100per" data-style="width:100%;height:12em;">
							<div class="sc_testimonial_item">
								<div class="sc_testimonial_avatar">
									<img alt="" src="http://placehold.it/70x70"></div>
									<div class="sc_testimonial_content">
										<p>Best purchase i made in envato. Great Theme!</p>
									</div>
									<div class="sc_testimonial_author">
										<a href="#">Sarah Jefferson</a>
									</div>
								</div>
							</div>
							<div class="swiper-slide height_12em width_100per" data-style="width:100%;height:12em;">
								<div class="sc_testimonial_item">
									<div class="sc_testimonial_avatar">
										<img alt="" src="http://placehold.it/70x70"></div>
										<div class="sc_testimonial_content">
											<p>Thank you for all your help and assistance over the years with our products.
												<br /> I would have no hesitation in recommending you to my clients.</p>
											</div>
											<div class="sc_testimonial_author">
												<a href="#">David Anderson</a>
											</div>
										</div>
									</div>
									<div class="swiper-slide height_12em width_100per" data-style="width:100%;height:12em;">
										<div class="sc_testimonial_item">
											<div class="sc_testimonial_content">
												<div class="sc_number aligncenter margin_bottom_1_5em">
													<span class="sc_number_item">4</span>
													<span class="sc_number_item">0</span>
													<span class="sc_number_item">0</span>
												</div> 
												faculty and staff teaching courses and discussing topics online
											</div>
											<div class="sc_testimonial_author">online Education</div>
										</div>
									</div>
								</div>
								<div class="sc_slider_controls_wrap">
									<a class="sc_slider_prev" href="#"></a>
									<a class="sc_slider_next" href="#"></a>
								</div>
							</div>
							<!-- /Testimonials section -->
						</div>
					</div>
				</footer>
				<!-- /Testimonials footer -->
				<footer class="contacts_wrap bg_tint_dark contacts_style_dark">
					<div class="content_wrap">
						<div class="logo">
							<strong><h3 style="font-size: 41px;font-weight: 500;">More Information</h3></strong>
						</div>
						<div class="contacts_address">
							<address class="address_right">
								Phone: 1.800.123.4567<br>
								Fax: 1.800.123.4566
							</address>
							<address class="address_left">
								San Francisco, CA 94102, US<br>	
								1234 Some St
							</address>
						</div>
						<div class="sc_socials sc_socials_size_big">
							<div class="sc_socials_item">
								<a href="#" target="_blank" class="social_icons social_facebook">
									<span class="sc_socials_hover social_facebook"></span>
								</a>
							</div>
							<div class="sc_socials_item">
								<a href="#" target="_blank" class="social_icons social_pinterest">
									<span class="sc_socials_hover social_pinterest"></span>
								</a>
							</div>
							<div class="sc_socials_item">
								<a href="#" target="_blank" class="social_icons social_twitter">
									<span class="sc_socials_hover social_twitter"></span>
								</a>
							</div>
							<div class="sc_socials_item">
								<a href="#" target="_blank" class="social_icons social_gplus">
									<span class="sc_socials_hover social_gplus"></span>
								</a>
							</div>
							<div class="sc_socials_item">
								<a href="#" target="_blank" class="social_icons social_rss">
									<span class="sc_socials_hover social_rss"></span>
								</a>
							</div>
							<div class="sc_socials_item">
								<a href="#" target="_blank" class="social_icons social_dribbble">
									<span class="sc_socials_hover social_dribbble"></span>
								</a>
							</div>
						</div>
					</div>
				</footer>
				<div class="copyright_wrap">
					<div class="content_wrap">
						<p>© <?php echo date('Y') ?> <strong><a href="#">HIMSI CIMONE</a></strong> All Rights Reserved. <a href="#">Terms of use</a> and <a href="#">Privacy Policy</a></p>
					</div>
				</div>