<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico" />
	<title>Homepage | Education Center</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.3.1" type="text/css" media="all" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,700,700italic&#038;subset=latin,latin-ext,cyrillic,cyrillic-ext" type="text/css" media="all" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister:400&#038;subset=latin" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/fontello/css/fontello.css') }}" type="text/css" media="all" />
	
	<link rel="stylesheet" href="{{ asset('frontend/js/rs-plugin/settings.css') }}" type="text/css" media="all" />

	<!-- <link rel="stylesheet" href="{{ asset('frontend/css/woocommerce/woocommerce-layout.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/woocommerce/woocommerce-smallscreen.css') }}" type="text/css" media="only screen and (max-width: 768px)" />
	<link rel="stylesheet" href="{{ asset('frontend/css/woocommerce/woocommerce.css') }}" type="text/css" media="all" /> -->

	<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/shortcodes.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/core.animation.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/tribe-style.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/skins/skin.css') }}" type="text/css" media="all" />

	<link rel="stylesheet" href="{{ asset('frontend/css/core.portfolio.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/js/mediaelement/mediaelementplayer.min.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/js/mediaelement/wp-mediaelement.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/js/prettyPhoto/css/prettyPhoto.css') }}" type="text/css" media="all" />	
	<link rel="stylesheet" href="{{ asset('frontend/js/core.customizer/front.customizer.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/js/core.messages/core.messages.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/js/swiper/idangerous.swiper.min.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/skins/skin-responsive.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/slider-style.css') }}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{ asset('frontend/css/custom-style.css') }}" type="text/css" media="all" />
</head>

<body class="home page body_style_fullscreen body_filled article_style_stretch layout_single-standard top_panel_style_dark top_panel_opacity_transparent top_panel_show top_panel_over menu_right user_menu_show sidebar_hide">
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-angle-double-up" data-url="" data-separator="yes"></a>
	<!-- Body -->
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			<!-- Header -->
			@include('frontend.includes.header')
			<!-- /Header -->

			<!-- /Content -->
			@yield('content')
			<!-- /Content -->

			<!-- Footer -->
			@include('frontend.includes.footer')
			<!-- /Footer -->
			@yield('modal')
		</div>
	</div>
	<!-- /Body -->
	<a href="#" class="scroll_to_top icon-up-2" title="Scroll to top"></a>
	<div class="custom_html_section"></div>

	<script type="text/javascript" src="{{ asset('frontend/js/jquery/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/jquery-migrate.min.js') }}"></script>	
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/ui/core.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/ui/widget.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/ui/tabs.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/ui/accordion.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/ui/effect.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/ui/effect-fade.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/jquery.blockUI.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery/jquery.cookie.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('frontend/js/global.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/core.utils.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/core.init.min.js') }}"></script>	
	<script type="text/javascript" src="{{ asset('frontend/js/shortcodes/shortcodes.min.js') }}"></script>	

	<script type="text/javascript" src="{{ asset('frontend/js/rs-plugin/jquery.themepunch.tools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/rs-plugin/jquery.themepunch.revolution.min.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('frontend/js/slider_init.js') }}"></script>

	<script type="text/javascript" src="{{ asset('frontend/js/superfish.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/jquery.slidemenu.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('frontend/js/mediaelement/mediaelement-and-player.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/mediaelement/wp-mediaelement.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('frontend/js/core.messages/core.messages.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('frontend/js/jquery.isotope.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/hover/jquery.hoverdir.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/prettyPhoto/jquery.prettyPhoto.min.js') }}"></script>		
	<script type="text/javascript" src="{{ asset('frontend/js/swiper/idangerous.swiper-2.7.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/swiper/idangerous.swiper.scrollbar-2.4.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/diagram/chart.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('frontend/js/core.customizer/front.customizer.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('frontend/js/skin.customizer.min.js') }}"></script>

</body>
</html>