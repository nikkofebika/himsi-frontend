@extends('frontend.layouts.blogs')

@section('content')
<div class="page_top_wrap page_top_title page_top_breadcrumbs">
	<div class="content_wrap">
		<div class="breadcrumbs">
			<a class="breadcrumbs_item home" href="index.html">Home</a>
			<span class="breadcrumbs_delimiter"></span>
			<a class="breadcrumbs_item all" href="blog-streampage.html">All posts</a>
			<span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">Our Blogs</span> 
		</div>
		<h1 class="page_title">Our Blogs</h1>
	</div>
</div>
<!-- /Page title -->
<!-- Content with sidebar -->
<div class="page_content_wrap">
	<div class="content_wrap">
		<!-- Content-->
		<div class="content">
			<!-- Video post -->
			@foreach($blogs as $blog)
			<article class="post_item post_item_excerpt even post type-post">
				<h3 class="post_title">
					<a href="{{ route('blog.show', $blog->id) }}">{{ $blog->title }}</a>
				</h3>
				<div class="post_featured">
					<div class="post_thumb" data-image="http://placehold.it/2300x1533" data-title="New Image Post">
						<a class="hover_icon hover_icon_link" href="{{ route('blog.show', $blog->id) }}">
							<img alt="" src="{{ asset('background_artikel/'.$blog->image) }}">
						</a>
					</div>
				</div>
				<div class="post_content clearfix">
					<div class="post_info">
						<span class="post_info_item post_info_posted">Posted 
							<a href="#" class="post_info_date">{{ $blog->created_at }}</a>
						</span>
						<span class="post_info_item post_info_posted_by">by 
							<a href="#" class="post_info_author">{{ $blog->posting_by }}</a>
						</span>
						<span class="post_info_item post_info_counters">	
							<a class="post_counters_item post_counters_views icon-eye" title="Views - 85" href="#">{{ $blog->view }}</a>
							<a class="post_counters_item post_counters_comments icon-comment-1" title="Comments - 0" href="#">
								<span class="post_counters_number">0</span>
							</a>
							<a class="post_counters_item post_counters_likes icon-heart enabled" title="Like" href="#">
								<span class="post_counters_number">{{ $blog->like }}</span>
							</a>
						</span>
					</div>
					<div class="post_descr">
						<p>{{ substr($blog->content, 0, 150) }}...</p>
						<a href="{{ route('blog.show', $blog->id) }}" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">READ MORE</a> 
					</div>
				</div>
			</article>
			@endforeach
			<!-- /Video post -->
		</div>
		<!-- /Content-->
		<!-- Sidebar -->
		<div class="sidebar widget_area bg_tint_light sidebar_style_white">
			<!-- Search widget -->
			<aside class="widget widget_search">
				<h5 class="widget_title">Search</h5>
				<form method="get" class="search_form" action="#">
					<input type="text" class="search_field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />
					<button type="submit" class="search_button icon-search-2"></button>
				</form>
			</aside>
			<!-- /Search widget -->
			<!-- Categories widget -->
			<aside class="widget">
				<h5 class="widget_title">Categories</h5>
				<ul>
					<li><a href="masonry-2-columns.html">Masonry (2 columns)</a> (12)
						<ul class="children">
							<li><a href="masonry-3-columns.html">Masonry (3 columns)</a> (12)</li>
						</ul>
					</li>
					<li><a href="portfolio-2-columns.html">Portfolio (2 columns)</a> (12)
						<ul class="children">
							<li><a href="portfolio-3-columns.html">Portfolio (3 columns)</a> (12)</li>
						</ul>
					</li>
					<li><a href="post-formats.html">Post Formats</a> (10)
						<ul class="children">
							<li><a href="post-formats-with-sidebar.html">Post Formats with Sidebar</a> (10)</li>
						</ul>
					</li>
				</ul>
			</aside>
			<!-- /Categories widget -->
			<!-- Recent posts widget -->
			<aside class="widget">
				<h5 class="widget_title">Recent Posts</h5>
				<article class="post_item first">
					<div class="post_thumb">
						<img alt="" src="http://placehold.it/75x75"></div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="post-with-sidebar.html">Medical Chemistry: The Molecular Basis</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="#" class="post_info_date">January 14, 2015</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="#" class="post_counters_item icon-eye"><span>29</span></a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75">
						</div>
						<div class="post_content">
							<h6 class="post_title">
								<a href="post-without-sidebar.html">Introduction to Computer  Science</a>
							</h6>
							<div class="post_info">
								<span class="post_info_item post_info_posted">
									<a href="#" class="post_info_date">January 14, 2015</a>
								</span>
								<span class="post_info_item post_info_posted_by">by 
									<a href="#" class="post_info_author">John Doe</a>
								</span>
								<span class="post_info_item post_info_counters">
									<a href="#" class="post_counters_item icon-eye"><span>0</span></a>
								</span>
							</div>
						</div>
					</article>
					<article class="post_item">
						<div class="post_thumb">
							<img alt="" src="http://placehold.it/75x75"></div>
							<div class="post_content">
								<h6 class="post_title">
									<a href="post-without-sidebar.html">Introduction to Biomedical Imaging</a>
								</h6>
								<div class="post_info">
									<span class="post_info_item post_info_posted">
										<a href="#" class="post_info_date">January 13, 2015</a>
									</span>
									<span class="post_info_item post_info_posted_by">by 
										<a href="#" class="post_info_author">John Doe</a>
									</span>
									<span class="post_info_item post_info_counters">
										<a href="#" class="post_counters_item icon-eye"><span>0</span></a>
									</span>
								</div>
							</div>
						</article>
						<article class="post_item">
							<div class="post_thumb">
								<img alt="" src="http://placehold.it/75x75"></div>
								<div class="post_content">
									<h6 class="post_title">
										<a href="post-without-sidebar.html">Evaluating Social  Programs</a>
									</h6>
									<div class="post_info">
										<span class="post_info_item post_info_posted">
											<a href="#" class="post_info_date">January 13, 2015</a>
										</span>
										<span class="post_info_item post_info_posted_by">by 
											<a href="#" class="post_info_author">John Doe</a>
										</span>
										<span class="post_info_item post_info_counters">
											<a href="#" class="post_counters_item icon-eye">
												<span class="post_counters_number">4</span>
											</a>
										</span>
									</div>
								</div>
							</article>
						</aside>
						<!-- /Recent posts widget -->
						<!-- Tag cloud widget -->
						<aside class="widget widget_tag_cloud">
							<h5 class="widget_title">Tags</h5>
							<div class="tagcloud">
								<a href="tag-page.html" title="1 topic">aside</a>
								<a href="tag-page.html" title="1 topic">audio</a>
								<a href="tag-page.html" title="1 topic">chat</a>
								<a href="tag-page.html" title="1 topic">computer</a>
								<a href="tag-page.html" title="2 topics">english</a>
								<a href="tag-page.html" title="1 topic">gallery</a>
								<a href="tag-page.html" title="1 topic">image</a>
								<a href="tag-page.html" title="1 topic">link</a>
								<a href="tag-page.html" title="10 topics">masonry</a>
								<a href="tag-page.html" title="2 topics">medical</a>
								<a href="tag-page.html" title="1 topic">quote</a>
								<a href="tag-page.html" title="2 topics">social</a>
								<a href="tag-page.html" title="1 topic">standard</a>
								<a href="tag-page.html" title="1 topic">status</a>
								<a href="tag-page.html" title="1 topic">video</a></div>
							</aside>
							<!-- /Tag cloud widget -->
							
							<!-- Popular posts widget -->
							<aside class="widget">
								<h5 class="widget_title">Popular</h5>
								<div class="sc_tabs sc_tabs_style_2" data-active="0">
									<ul class="sc_tabs_titles">
										<li class="sc_tabs_title first">
											<a href="#sc_tabs_1" class="theme_button">Views</a>
										</li>
										<li class="sc_tabs_title">
											<a href="#sc_tabs_2" class="theme_button">Comments</a>
										</li>
										<li class="sc_tabs_title last">
											<a href="#sc_tabs_3" class="theme_button">Likes</a>
										</li>
									</ul>
									<div id="sc_tabs_1" class="sc_tabs_content odd first">
										<article class="post_item first">
											<div class="post_thumb">
												<img alt="" src="http://placehold.it/75x75">
											</div>
											<div class="post_content">
												<h6 class="post_title">
													<a href="post-with-sidebar.html">Medical Chemistry: The Molecular Basis</a>
												</h6>
												<div class="post_info">
													<span class="post_info_item post_info_posted">
														<a href="#" class="post_info_date">January 14, 2015</a>
													</span>
													<span class="post_info_item post_info_posted_by">by 
														<a href="#" class="post_info_author">John Doe</a>
													</span>
													<span class="post_info_item post_info_counters">
														<a href="#" class="post_counters_item post_counters_views icon-eye"><span>29</span></a>
													</span>
												</div>
											</div>
										</article>
										<article class="post_item">
											<div class="post_thumb">
												<img alt="" src="http://placehold.it/75x75"></div>
												<div class="post_content">
													<h6 class="post_title">
														<a href="post-audio-format.html">Audio Post Format</a>
													</h6>
													<div class="post_info">
														<span class="post_info_item post_info_posted">
															<a href="#" class="post_info_date">December 30, 2014</a>
														</span>
														<span class="post_info_item post_info_posted_by">by 
															<a href="#" class="post_info_author">John Doe</a>
														</span>
														<span class="post_info_item post_info_counters">
															<a href="#" class="post_counters_item post_counters_views icon-eye"><span>27</span></a>
														</span>
													</div>
												</div>
											</article>
											<article class="post_item with_thumb">
												<div class="post_content">
													<h6 class="post_title">
														<a href="post-without-image.html">New Post Without Image</a>
													</h6>
													<div class="post_info">
														<span class="post_info_item post_info_posted">
															<a href="#" class="post_info_date">December 28, 2014</a>
														</span>
														<span class="post_info_item post_info_posted_by">by 
															<a href="#" class="post_info_author">John Doe</a></span>
															<span class="post_info_item post_info_counters">
																<a href="#" class="post_counters_item post_counters_views icon-eye"><span>25</span></a>
															</span>
														</div>
													</div>
												</article>
												<article class="post_item">
													<div class="post_thumb">
														<img alt="" src="http://placehold.it/75x75"></div>
														<div class="post_content">
															<h6 class="post_title">
																<a href="post-with-video.html">Our Video Training  for Microsoft products  and technologies</a>
															</h6>
															<div class="post_info">
																<span class="post_info_item post_info_posted">
																	<a href="#" class="post_info_date">December 30, 2014</a>
																</span>
																<span class="post_info_item post_info_posted_by">by 
																	<a href="#" class="post_info_author">John Doe</a>
																</span>
																<span class="post_info_item post_info_counters">
																	<a href="#" class="post_counters_item post_counters_views icon-eye"><span>23</span></a>
																</span>
															</div>
														</div>
													</article>
												</div>
												<div id="sc_tabs_2" class="sc_tabs_content even">
													<article class="post_item first">
														<div class="post_content">
															<h6 class="post_title">
																<a href="post-without-image.html">New Post Without Image</a>
															</h6>
															<div class="post_info">
																<span class="post_info_item post_info_posted">
																	<a href="#" class="post_info_date">December 28, 2014</a>
																</span>
																<span class="post_info_item post_info_posted_by">by 
																	<a href="#" class="post_info_author">John Doe</a>
																</span>
																<span class="post_info_item post_info_counters">
																	<a href="#" class="post_counters_item post_counters_comments icon-comment-1"><span>4</span></a>
																</span>
															</div>
														</div>
													</article>
													<article class="post_item">
														<div class="post_thumb">
															<img alt="" src="http://placehold.it/75x75">
														</div>
														<div class="post_content">
															<h6 class="post_title">
																<a href="post-with-video.html">Our Video Training  for Microsoft products  and technologies</a>
															</h6>
															<div class="post_info">
																<span class="post_info_item post_info_posted">
																	<a href="#" class="post_info_date">December 30, 2014</a>
																</span>
																<span class="post_info_item post_info_posted_by">by 
																	<a href="#" class="post_info_author">John Doe</a>
																</span>
																<span class="post_info_item post_info_counters">
																	<a href="#" class="post_counters_item post_counters_comments icon-comment-1"><span>1</span></a>
																</span>
															</div>
														</div>
													</article>
													<article class="post_item">
														<div class="post_thumb">
															<img alt="" src="http://placehold.it/75x75"></div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-audio-format.html">Audio Post Format</a>
																</h6>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">December 30, 2014</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																	<span class="post_info_item post_info_counters">
																		<a href="#" class="post_counters_item post_counters_comments icon-comment-1"><span>1</span>
																		</a>
																	</span>
																</div>
															</div>
														</article>
														<article class="post_item">
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-aside-format.html">Aside Format</a>
																</h6>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">December 10, 2014</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																	<span class="post_info_item post_info_counters">
																		<a href="#" class="post_counters_item post_counters_comments icon-comment-1"><span>0</span></a>
																	</span>
																</div>
															</div>
														</article>
													</div>
													<div id="sc_tabs_3" class="sc_tabs_content odd">
														<article class="post_item first">
															<div class="post_thumb">
																<img alt="" src="http://placehold.it/75x75">
															</div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-with-video.html">Our Video Training  for Microsoft products  and technologies</a>
																</h6>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">December 30, 2014</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																	<span class="post_info_item post_info_counters">
																		<a href="#" class="post_counters_item post_counters_likes icon-heart-1"><span>1</span></a>
																	</span>
																</div>
															</div>
														</article>
														<article class="post_item with_thumb">
															<div class="post_thumb">
																<img alt="" src="http://placehold.it/75x75">
															</div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-without-sidebar.html">Introduction to Biomedical Imaging</a>
																</h6>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">January 13, 2015</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																	<span class="post_info_item post_info_counters">
																		<a href="#" class="post_counters_item post_counters_likes icon-heart-1"><span>0</span></a>
																	</span>
																</div>
															</div>
														</article>
														<article class="post_item">
															<div class="post_thumb">
																<img alt="" src="http://placehold.it/75x75">
															</div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-without-sidebar.html">Evaluating Social  Programs</a>
																</h6>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">January 13, 2015</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																	<span class="post_info_item post_info_counters">
																		<a href="#" class="post_counters_item post_counters_likes icon-heart-1"><span>0</span></a>
																	</span>
																</div>
															</div>
														</article>
														<article class="post_item">
															<div class="post_thumb">
																<img alt="" src="http://placehold.it/75x75">
															</div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-audio-format.html">Audio Post Format</a>
																</h6>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">December 30, 2014</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																	<span class="post_info_item post_info_counters">
																		<a href="#" class="post_counters_item post_counters_likes icon-heart-1"><span>0</span></a>
																	</span>
																</div>
															</div>
														</article>
													</div>
												</div>
											</aside>
											<!-- /Popular posts widget -->
											<!-- Socials widget -->
											<aside class="widget widget_socials">
												<h5 class="widget_title">Follow Us</h5>
												<div class="widget_inner">
													<div class="logo_descr">Curabitur massa odio vulputate vel fringilla vitae posuere.</div>
													<div class="sc_socials sc_socials_size_small">
														<div class="sc_socials_item">
															<a href="#" target="_blank" class="social_icons social_facebook">
																<span class="sc_socials_hover social_facebook"></span>
															</a>
														</div>
														<div class="sc_socials_item">
															<a href="#" target="_blank" class="social_icons social_pinterest">
																<span class="sc_socials_hover social_pinterest"></span>
															</a>
														</div>
														<div class="sc_socials_item">
															<a href="#" target="_blank" class="social_icons social_twitter">
																<span class="sc_socials_hover social_twitter"></span>
															</a>
														</div>
														<div class="sc_socials_item">
															<a href="#" target="_blank" class="social_icons social_gplus">
																<span class="sc_socials_hover social_gplus"></span>
															</a>
														</div>
														<div class="sc_socials_item">
															<a href="#" target="_blank" class="social_icons social_rss">
																<span class="sc_socials_hover social_rss"></span>
															</a>
														</div>
														<div class="sc_socials_item">
															<a href="#" target="_blank" class="social_icons social_dribbble">
																<span class="sc_socials_hover social_dribbble"></span>
															</a>
														</div>
													</div>
												</div>
											</aside>
											<!-- /Socials widget -->
											<!-- Top 10 widget -->
											<aside class="widget">
												<h5 class="widget_title">Rating</h5>
												<article class="post_item first">
													<div class="post_thumb">
														<img alt="" src="http://placehold.it/75x75"></div>
														<div class="post_content">
															<h6 class="post_title">
																<a href="post-with-sidebar.html">Medical Chemistry: The Molecular Basis</a>
															</h6>
															<div class="post_rating reviews_summary blog_reviews">
																<div class="criteria_summary criteria_row">
																	<div class="reviews_stars reviews_style_stars" data-mark="84.0">
																		<div class="reviews_stars_wrap">
																			<div class="reviews_stars_bg">
																				<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span></div>
																				<div class="reviews_stars_hover width_84per">
																					<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span>
																				</div>
																			</div>
																			<div class="reviews_value">84.0</div>
																		</div>
																	</div>
																</div>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">January 14, 2015</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																</div>
															</div>
														</article>
														<article class="post_item">
															<div class="post_thumb">
																<img alt="" src="http://placehold.it/75x75">
															</div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-without-sidebar.html">Entrepreneurship 101:  Who is your customer?</a>
																</h6>
																<div class="post_rating reviews_summary blog_reviews">
																	<div class="criteria_summary criteria_row">
																		<div class="reviews_stars reviews_style_stars" data-mark="80.5">
																			<div class="reviews_stars_wrap">
																				<div class="reviews_stars_bg">
																					<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span>
																				</div>
																				<div class="reviews_stars_hover width_81per">
																					<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span>
																				</div>
																			</div>
																			<div class="reviews_value">80.5</div>
																		</div>
																	</div>
																</div>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">January 11, 2015</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																</div>
															</div>
														</article>
														<article class="post_item">
															<div class="post_thumb">
																<img alt="" src="http://placehold.it/75x75">
															</div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-with-video.html">Our Video Training  for Microsoft products  and technologies</a>
																</h6>
																<div class="post_rating reviews_summary blog_reviews">
																	<div class="criteria_summary criteria_row">
																		<div class="reviews_stars reviews_style_stars" data-mark="77.8">
																			<div class="reviews_stars_wrap">
																				<div class="reviews_stars_bg">
																					<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span>
																				</div>
																				<div class="reviews_stars_hover width_78per">
																					<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span>
																				</div>
																			</div>
																			<div class="reviews_value">77.8</div>
																		</div>
																	</div>
																</div>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">December 30, 2014</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																</div>
															</div>
														</article>
														<article class="post_item with_thumb">
															<div class="post_thumb">
																<img alt="" src="http://placehold.it/75x75">
															</div>
															<div class="post_content">
																<h6 class="post_title">
																	<a href="post-audio-format.html">Audio Post Format</a>
																</h6>
																<div class="post_rating reviews_summary blog_reviews">
																	<div class="criteria_summary criteria_row">
																		<div class="reviews_stars reviews_style_stars" data-mark="67.3">
																			<div class="reviews_stars_wrap">
																				<div class="reviews_stars_bg">
																					<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span>
																				</div>
																				<div class="reviews_stars_hover width_67per">
																					<span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span><span class="reviews_star"></span>
																				</div>
																			</div>
																			<div class="reviews_value">67.3</div>
																		</div>
																	</div>
																</div>
																<div class="post_info">
																	<span class="post_info_item post_info_posted">
																		<a href="#" class="post_info_date">December 30, 2014</a>
																	</span>
																	<span class="post_info_item post_info_posted_by">by 
																		<a href="#" class="post_info_author">John Doe</a>
																	</span>
																</div>
															</div>
														</article>
													</aside>
													<!-- /Top 10 widget -->
												</div>
												<!-- /Sidebar -->
											</div>
										</div>
										@endsection