@extends('frontend.layouts.blogs')

@section('content')
<!-- Page title -->
<div class="page_top_wrap page_top_title page_top_breadcrumbs">
	<div class="content_wrap">
		<div class="breadcrumbs">
			<a class="breadcrumbs_item home" href="/">Home</a>
			<span class="breadcrumbs_delimiter"></span>
			<a class="breadcrumbs_item all" href="{{ route('frontblog') }}">All posts</a>
			<span class="breadcrumbs_delimiter"></span>
			<span class="breadcrumbs_item current">{{ $blog->title }}</span> 
		</div>
		<h1 class="page_title">{{ $blog->title }}</h1>
	</div>
</div>
<!-- /Page title -->
<!-- Content -->
<div class="page_content_wrap">
	<div class="content_wrap">
		<div class="content">
			<article class="post_item post_item_single post_featured_right post">
				<section class="post_featured">
					<div class="post_thumb" data-image="{{ asset('background_artikel/'. $blog->image) }}" data-title="{{ $blog->title }}">
						<a class="hover_icon hover_icon_view" href="http://placehold.it/2300x1533" title="New Image Post">
							<img alt="{{ $blog->title }}" src="{{ asset('background_artikel/'. $blog->image) }}">
						</a>
					</div>
				</section>
				<div class="post_info">
					<span class="post_info_item post_info_posted">Posted 
						<a href="post-image.html" class="post_info_date date updated" content="2014-12-27">{{ date('d-m-Y', strtotime($blog->created_at)) }}</a>
					</span>
					<span class="post_info_item post_info_posted_by">by 
						<a href="#" class="post_info_author">{{ $blog->posting_by }}</a>
					</span>
					<span class="post_info_item post_info_counters">	
						<span class="post_counters_item post_counters_views icon-eye" title="Views - 94">{{ $blog->view }}</span>
						<a class="post_counters_item post_counters_comments icon-comment-1" title="Comments - 0" href="#">
							<span class="post_counters_number">0</span>
						</a>
						<a class="post_counters_item post_counters_likes icon-heart enabled" title="Like" href="#">
							<span class="post_counters_number">{{ $blog->like }}</span>
						</a>
					</span>
				</div>
				<section class="post_content">
					{{ $blog->content }}
					<div class="post_info post_info_bottom">
						<span class="post_info_item post_info_tags">Tags: 
							<a class="post_tag_link" href="tag-page.html">{{ $blog->tags }}</a>
						</span>
					</div>
				</section>
				<section class="post_author author">
					<div class="post_author_avatar">
						<a href="#">
							<img alt="" src="http://1.gravatar.com/avatar/45e4d63993e55fa97a27d49164bce80f?s=75&#038;d=mm&#038;r=g" srcset="http://1.gravatar.com/avatar/45e4d63993e55fa97a27d49164bce80f?s=150&amp;d=mm&amp;r=g 2x" class="avatar avatar-75 photo" height="75" width="75" /></a>
						</div>
						<h6 class="post_author_title">About 
							<span><a href="#" class="fn">John Doe</a></span>
						</h6>
						<div class="post_author_info"></div>
					</section>
				</article>
			</div>
		</div>
		<div class="content_wrap">
			<div class="content">
				<!-- Comments form section -->
				<section class="comments_wrap">
					<div class="comments_form_wrap">
						<h2 class="section_title comments_form_title">Add Comment</h2>
						<div class="comments_form">
							<div id="respond" class="comment-respond">
								<form action="#" method="post" id="commentform" class="comment-form">
									<p class="comments_notes">Your email address will not be published. Required fields are marked *</p>
									<div class="columns_wrap">
										<div class="comments_field comments_author column-1_2">
											<label for="author" class="required">Name</label>
											<input id="author" name="author" type="text" placeholder="Name *" value="" size="30" aria-required="true" />
										</div>
										<div class="comments_field comments_email column-1_2">
											<label for="email" class="required">Email</label>
											<input id="email" name="email" type="text" placeholder="Email *" value="" size="30" aria-required="true" />
										</div>
										<div class="comments_field comments_site column-1_1">
											<label for="url" class="optional">Website</label>
											<input id="url" name="url" type="text" placeholder="Website" value="" size="30" aria-required="true" />
										</div>
									</div>
									<div class="comments_field comments_message">
										<label for="comment" class="required">Your Message</label>
										<textarea id="comment" name="comment" placeholder="Comment" aria-required="true"></textarea>
									</div>
									<p class="form-submit">
										<input name="submit" type="submit" id="send_comment" class="submit" value="Post Comment" />
									</p>
								</form>
							</div>
						</div>
					</div>
				</section>
				<!-- /Comments form section -->
			</div>
		</div>
	</div>
	<!-- /Content -->	
	@endsection