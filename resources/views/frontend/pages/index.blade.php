@extends('frontend.layouts.master')

@section('content')
<section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_education_home_slider">
	<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">                    
		<div id="rev_slider_1_1" class="rev_slider fullwidthabanner disp_none height_630 max-height_630">
			<ul>
				<!-- Slide 1 -->
				<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
					<img src="{{ asset('frontend/images/green.jpg') }}" alt="green" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
					<div class="tp-caption customin stl cust-z-index-5 rs-cust-style8" data-x="20" data-y="230" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300" style="border-radius: 5px; border: 5px solid #ffffff;">
						<img src="{{ asset('frontend/images/sliders/slider11.jpg') }}" alt="">
					</div>
					<div class="tp-caption title sfr stl tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="190" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Take great courses from the world's best universities
					</div>
					<div class="tp-caption slide_text sfr stl tp-resizeme cust-z-index-7 rs-cust-style2" data-x="570" data-y="380" data-speed="500" data-start="1750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8500" data-endspeed="300">
						<span class="font-w_400">Our courses are built in partnership with technology leaders and are relevant to industry needs.</span>
					</div>
					<div class="tp-caption slide_button customin stl tp-resizeme cust-z-index-8 rs-cust-style3" data-x="570" data-y="460" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:-20;transformOrigin:50% 0%;" data-speed="500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8600" data-endspeed="300">
						<a href="#" class="slide_button_white">Start Learning Now</a>
					</div>
				</li>
				<!-- Slide 2 -->
				<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
					<img src="{{ asset('frontend/images/green.jpg') }}" alt="blue" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
					<div class="tp-caption customin stl cust-z-index-5 rs-cust-style8" data-x="40" data-y="200" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:360;scaleX:0.1;scaleY:0.1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
						<img src="{{ asset('frontend/images/sliders/slider33.jpeg') }}" alt="">
					</div>
					<div class="tp-caption title sfb stb tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="200" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Education Center
						<br> and distance education
					</div>
					<div class="tp-caption slide_text sfb stb tp-resizeme cust-z-index-7 rs-cust-style2" data-x="570" data-y="355" data-speed="500" data-start="1750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8500" data-endspeed="300">
						<span class="font-w_400">Online Education leads the world in distance education with high quality online degrees and online courses.</span>
					</div>
					<div class="tp-caption slide_button customin stb tp-resizeme cust-z-index-8 rs-cust-style3" data-x="570" data-y="460" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8600" data-endspeed="300">
						<a href="#" class="slide_button_white">Start Learning Now</a>
					</div>
				</li>
				<!-- Slide 3 -->
				<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
					<img src="{{ asset('frontend/images/green.jpg') }}" alt="yellow" data-bgposition="center top" data-bgfit="normal" data-bgrepeat="repeat">
					<div class="tp-caption roundedimage sfl stl cust-z-index-5 rs-cust-style8" data-x="50" data-y="200" data-speed="1300" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8250" data-endspeed="300">
						<img src="{{ asset('frontend/images/sliders/slider22.jpg') }}" alt="">
					</div>
					<div class="tp-caption title customin stb tp-resizeme cust-z-index-6 rs-cust-style1" data-x="570" data-y="200" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="500" data-start="1350" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8400" data-endspeed="300">Receive personalized
						<br> coaching
					</div>
					<div class="tp-caption slide_text customin stb tp-resizeme cust-z-index-7 rs-cust-style2" data-x="570" data-y="355" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="500" data-start="1750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8500" data-endspeed="300" >
						<span class="font-w_400">Learning is a collaborative process, and we're here to provide you with guidance every step of the way.</span>
					</div>
					<div class="tp-caption slide_button customin stb tp-resizeme cust-z-index-8 rs-cust-style3" data-x="570" data-y="460" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:20;transformOrigin:50% 100%;" data-speed="500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="8600" data-endspeed="300">
						<a href="#" class="slide_button_white">Start Learning Now</a>
					</div>
				</li>
			</ul>
			<div class="tp-bannertimer"></div>
		</div>
	</div>
</section>
<!-- Revolution slider -->
<!-- Content -->
<div class="page_content_wrap">
	<div class="content">
		<article class="post_item post_item_single page">
			<section class="post_content">
				<!-- Features section -->
				<div class="sc_section" data-animation="animated zoomIn normal">
					<div class="sc_content content_wrap margin_top_3em_imp margin_bottom_3em_imp sc_features_st1">
						<div class="columns_wrap sc_columns columns_fluid sc_columns_count_3">
							<div class="column-1_3 sc_column_item sc_column_item_1 odd first text_center">
								<a href="#" class="sc_icon icon-woman-2 sc_icon_bg_menu menu_dark_color font_5em lh_1em"></a>
								<div class="sc_section font-w_400 margin_top_1em_imp">
									<p>
										<a class="menu_color" href="#">Take computer science courses<br />
										with personalized support</a>
									</p>
								</div>
							</div>
							<div class="column-1_3 sc_column_item sc_column_item_2 even text_center">
								<a href="#" class="sc_icon icon-rocket-2 sc_icon_bg_menu menu_dark_color font_5em lh_1em"></a>
								<div class="sc_section font-w_400 margin_top_1em_imp">
									<p>
										<a class="menu_color" href="#">Build cool projects<br />
										to showcase your skills</a>
									</p>
								</div>
							</div>
							<div class="column-1_3 sc_column_item sc_column_item_3 odd text_center">
								<a href="#" class="sc_icon icon-world-2 sc_icon_bg_menu menu_dark_color font_5em lh_1em"></a>
								<div class="sc_section font-w_400 margin_top_1em_imp">
									<p>
										<a class="menu_color" href="#">Earn certificates<br />
										recognized by Industry</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Features section -->			
				<!-- Courses section -->
				<div class="sc_section accent_top bg_tint_light sc_bg1" data-animation="animated fadeInUp normal">
					<div class="sc_section_overlay">
						<div class="sc_section_content">
							<div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp">
								<h2 class="sc_title sc_title_regular sc_align_center margin_top_0 margin_bottom_085em text_center">Courses Starting Soon</h2>
								<div class="sc_blogger layout_courses_3 template_portfolio sc_blogger_horizontal no_description">
									<div class="isotope_wrap" data-columns="3">
										<!-- Courses item -->
										<div class="isotope_item isotope_item_courses isotope_column_3">
											<div class="post_item post_item_courses odd">
												<div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
													<div class="post_featured img">
														<a href="paid-course.html">
															<img alt="" src="http://placehold.it/400x400">
														</a>
														<h4 class="post_title">
															<a href="paid-course.html">Principles of Written English, Part 2</a>
														</h4>
														<div class="post_descr">
															<div class="post_price">
																<span class="post_price_value">$85</span>
																<span class="post_price_period">monthly</span>
															</div>
															<div class="post_category">
																<a href="product-category.html">Language</a>
															</div>
														</div>
													</div>
													<div class="post_info_wrap info">
														<div class="info-back">
															<h4 class="post_title">
																<a href="paid-course.html">Principles of Written English, Part 2</a>
															</h4>
															<div class="post_descr">
																<p>
																	<a href="paid-course.html">Nam id leo massa. Cras at condimentum nisi, vulputate ultrices turpis.</a>
																</p>
																<div class="post_buttons">
																	<div class="post_button">
																		<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
																	</div>
																	<div class="post_button">
																		<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- /Courses item -->
										<!-- Courses item -->
										<div class="isotope_item isotope_item_courses isotope_column_3">
											<div class="post_item post_item_courses even">
												<div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
													<div class="post_featured img">
														<a href="paid-course.html">
															<img alt="" src="http://placehold.it/400x400">
														</a>
														<h4 class="post_title">
															<a href="paid-course.html">Entrepreneurship 101: Who is your customer?</a>
														</h4>
														<div class="post_descr">
															<div class="post_price">
																<span class="post_price_value">$195</span><span class="post_price_period">monthly</span>
															</div>
															<div class="post_category">
																<a href="product-category.html">Marketing</a>
															</div>
														</div>
													</div>
													<div class="post_info_wrap info">
														<div class="info-back">
															<h4 class="post_title">
																<a href="paid-course.html">Entrepreneurship 101:  Who is your customer?</a>
															</h4>
															<div class="post_descr">
																<p>
																	<a href="paid-course.html">Quisque a nulla eget ante vestibulum lacinia eu quis massa.</a>
																</p>
																<div class="post_buttons">
																	<div class="post_button">
																		<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
																	</div>
																	<div class="post_button">
																		<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- /Courses item -->
										<!-- Courses item -->
										<div class="isotope_item isotope_item_courses isotope_column_3">
											<div class="post_item post_item_courses odd">
												<div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
													<div class="post_featured img">
														<a href="free-course.html">
															<img alt="" src="http://placehold.it/400x400">
														</a>
														<h4 class="post_title">
															<a href="#">Evaluating Social Programs</a>
														</h4>
														<div class="post_descr">
															<div class="post_price">
																<span class="post_price_value">Free!</span>
															</div>
															<div class="post_category">
																<a href="product-category.html">Social</a>
															</div>
														</div>
													</div>
													<div class="post_info_wrap info">
														<div class="info-back">
															<h4 class="post_title">
																<a href="free-course.html">Evaluating Social  Programs</a>
															</h4>
															<div class="post_descr">
																<p>
																	<a href="#">Nunc finibus vestibulum dui a fringilla. Maecenas maximus in massa sit amet maximus.</a>
																</p>
																<div class="post_buttons">
																	<div class="post_button">
																		<a href="free-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- /Courses item -->
										<!-- Courses item -->
										<div class="isotope_item isotope_item_courses isotope_column_3">
											<div class="post_item post_item_courses even">
												<div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
													<div class="post_featured img">
														<a href="paid-course.html">
															<img alt="" src="http://placehold.it/400x400">
														</a>
														<h4 class="post_title">
															<a href="#">Principles of Written English, Part 1</a>
														</h4>
														<div class="post_descr">
															<div class="post_price">
																<span class="post_price_value">$85</span>
															</div>
															<div class="post_category">
																<a href="product-category.html">Language</a>
															</div>
														</div>
													</div>
													<div class="post_info_wrap info">
														<div class="info-back">
															<h4 class="post_title">
																<a href="paid-course.html">Principles of Written English, Part 1</a>
															</h4>
															<div class="post_descr">
																<p>
																	<a href="paid-course.html">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</a>
																</p>
																<div class="post_buttons">
																	<div class="post_button">
																		<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
																	</div>
																	<div class="post_button">
																		<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- /Courses item -->
										<!-- Courses item -->
										<div class="isotope_item isotope_item_courses isotope_column_3">
											<div class="post_item post_item_courses odd">
												<div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
													<div class="post_featured img">
														<a href="paid-course.html">
															<img alt="" src="http://placehold.it/400x400"></a>
															<h4 class="post_title">
																<a href="paid-course.html">Introduction to Biomedical Imaging</a>
															</h4>
															<div class="post_descr">
																<div class="post_price">
																	<span class="post_price_value">$400</span>
																</div>
																<div class="post_category">
																	<a href="product-category.html">Medicine</a>
																</div>
															</div>
														</div>
														<div class="post_info_wrap info">
															<div class="info-back">
																<h4 class="post_title">
																	<a href="paid-course.html">Introduction to Biomedical Imaging</a>
																</h4>
																<div class="post_descr">
																	<p><a href="paid-course.html">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></p>
																	<div class="post_buttons">
																		<div class="post_button">
																			<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
																		</div>
																		<div class="post_button">
																			<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- /Courses item -->
											<!-- Courses item -->
											<div class="isotope_item isotope_item_courses isotope_column_3">
												<div class="post_item post_item_courses even last">
													<div class="post_content isotope_item_content ih-item colored square effect_dir left_to_right">
														<div class="post_featured img">
															<a href="paid-course.html">
																<img alt="" src="http://placehold.it/400x400">
															</a>
															<h4 class="post_title">
																<a href="paid-course.html">Introduction to Computer  Science</a>
															</h4>
															<div class="post_descr">
																<div class="post_price">
																	<span class="post_price_value">$120</span>
																	<span class="post_price_period">monthly</span>
																</div>
																<div class="post_category">
																	<a href="product-category.html">Computers</a>
																</div>
															</div>
														</div>
														<div class="post_info_wrap info">
															<div class="info-back">
																<h4 class="post_title">
																	<a href="paid-course.html">Introduction to Computer  Science</a>
																</h4>
																<div class="post_descr">
																	<p><a href="paid-course.html">Sed interdum felis diam, vitae rutrum urna laoreet vehicula.</a></p>
																	<div class="post_buttons">
																		<div class="post_button">
																			<a href="paid-course.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">LEARN MORE</a>
																		</div>
																		<div class="post_button">
																			<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">BUY NOW</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- /Courses item -->
										</div>
									</div>
									<a href="courses-streampage.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_menu sc_button_size_small aligncenter sc_button_iconed icon-graduation margin_top_1em margin_bottom_4 widht_12em">VIEW ALL COURSES</a>
								</div>
							</div>
						</div>
					</div>
					<!-- /Courses section -->
					<!-- Partners section -->
					<div class="sc_section" data-animation="animated fadeInUp normal">
						<div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp">
							<div class="sc_section aligncenter width_70per">
								<h2 class="sc_title sc_title_regular margin_top_0">Learn From the Best</h2>
								<p>Our online courses are built in partnership with technology leaders and are relevant to industry needs.
									<br /> Upon completing a Online course, you&#8217;ll receive a verified completion certificate recognized by industry leaders.
								</p>
							</div>
							<div id="sc_section_4" class="sc_section margin_top_1_5em_imp margin_bottom_0_imp height_75">
								<div id="sc_section_4_scroll" class="sc_scroll sc_scroll_horizontal swiper-slider-container scroll-container height_75">
									<div class="sc_scroll_wrapper swiper-wrapper">
										<div class="sc_scroll_slide swiper-slide">
											<figure class="sc_image  alignleft sc_image_shape_square margin_right_0_imp">
												<img src="http://placehold.it/56x60" alt="" />
											</figure>
											<figure class="sc_image  alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
												<img src="http://placehold.it/159x60" alt="" />
											</figure>
											<figure class="sc_image  alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
												<img src="http://placehold.it/83x60" alt="" />
											</figure>
											<figure class="sc_image  alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
												<img src="http://placehold.it/62x60" alt="" />
											</figure>
											<figure class="sc_image  alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
												<img src="http://placehold.it/60x60" alt="" />
											</figure>
											<figure class="sc_image  alignleft sc_image_shape_square margin_right_0_imp margin_left_4em_imp">
												<img src="http://placehold.it/155x60" alt="" />
											</figure>
										</div>
									</div>
									<div id="sc_section_4_scroll_bar" class="sc_scroll_bar sc_scroll_bar_horizontal sc_section_471175375_scroll_bar"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Partners section -->
					<!-- Video training section -->			
					<div class="sc_line sc_line_style_solid margin_top_0 margin_bottom_0"></div>
					<div class="sc_section" data-animation="animated fadeInUp normal">
						<div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp ">
							<div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2">
								<div class="column-1_2 sc_column_item sc_column_item_1 odd first res_width_100per_imp">
									<h3 class="sc_title sc_title_iconed sc_align_left text_left">
										<span class="sc_title_icon sc_title_icon_top sc_title_icon_medium icon-video-2"></span>
										Our Video Training for Microsoft products and technologies
									</h3>
									<p>Mauris vitae quam ligula. In tincidunt sapien sed nibh scelerisque congue. Maecenas ut libero eu metus tincidunt lobortis. Duis accumsan at mauris vel lacinia.</p>
									<a href="courses-streampage.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_mini sc_button_iconed inherit margin_top_1em margin_bottom_4 margin_left_4">BROWSE COURSES</a>
								</div>
								<div class="column-1_2 sc_column_item sc_column_item_2 even res_width_100per_imp">
									<div class="sc_video_player sc_video_bordered sc_video_st1">
										<div class="sc_video_frame sc_video_play_button hover_icon hover_icon_play width_100per" data-width="100%" data-height="647" data-video="&lt;iframe class=&quot;video_frame&quot; src=&quot;http://youtube.com/embed/636Dp8eHWnM?autoplay=1&quot; width=&quot;100%&quot; height=&quot;647&quot; frameborder=&quot;0&quot; webkitAllowFullScreen=&quot;webkitAllowFullScreen&quot; mozallowfullscreen=&quot;mozallowfullscreen&quot; allowFullScreen=&quot;allowFullScreen&quot;&gt;&lt;/iframe&gt;">
											<img alt="" src="http://placehold.it/400x225">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Video training section -->						
					<!-- Pricing section -->
					<div class="sc_section accent_top bg_tint_light sc_bg1" data-animation="animated fadeInUp normal">
						<div class="sc_section_overlay">
							<div class="sc_section_content">
								<div class="sc_content content_wrap margin_top_2_5em_imp margin_bottom_2_5em_imp">
									<h2 class="sc_title sc_title_regular sc_align_center text_center margin_top_0 margin_bottom_085em">Plans &amp; Pricing</h2>
									<div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3">
										<div class="column-1_3 sc_column_item sc_column_item_1 odd first text_center">
											<div class="sc_price_block sc_price_block_style_1 width_100per">
												<div class="sc_price_block_title">Trial</div>
												<div class="sc_price_block_money">
													<div class="sc_price_block_icon icon-clock-2"></div>
												</div>
												<div class="sc_price_block_description">
													<span class="sc_highlight font_2_57em lh_1em"><b>Free!</b> 30 Days</span>
												</div>
												<div class="sc_price_block_link">
													<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">I WANT THIS PLAN</a>
												</div>
											</div>
										</div>
										<div class="column-1_3 sc_column_item sc_column_item_2 even text_center">
											<div class="sc_price_block sc_price_block_style_2">
												<div class="sc_price_block_title">Monthly</div>
												<div class="sc_price_block_money">
													<div class="sc_price"><span class="sc_price_currency">$</span><span class="sc_price_money">89</span></div>
												</div>
												<div class="sc_price_block_description">
													<p><b>Save $98</b> every year compared to the monthly
														<br /> plan by paying yearly.</p>
													</div>
													<div class="sc_price_block_link">
														<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">I WANT THIS PLAN</a>
													</div>
												</div>
											</div>
											<div class="column-1_3 sc_column_item sc_column_item_3 odd text_center">
												<div class="sc_price_block sc_price_block_style_3">
													<div class="sc_price_block_title">Yearly</div>
													<div class="sc_price_block_money">
														<div class="sc_price">
															<span class="sc_price_currency">$</span>
															<span class="sc_price_money">129</span>
														</div>
													</div>
													<div class="sc_price_block_description">
														<p><b>Save $120</b> every year compared to the monthly
															<br /> plan by paying biannually.</p>
														</div>
														<div class="sc_price_block_link">
															<a href="product-page.html" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small">I WANT THIS PLAN</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Pricing section -->
						</section>
					</article>
				</div>
			</div>
			@endsection