<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// FRONTEND
Route::group(['middleware' => 'web'], function(){

	Route::get('/', 'FrontController@index');
	Route::get('/tes', 'FrontController@tes');
	Route::get('/frontblog', 'FrontblogController@index')->name('frontblog');
	Route::get('/blog', 'FrontblogController@blog')->name('blog');
	Route::get('/blog/{id}', 'FrontblogController@show')->name('blog.show');
	Route::get('/join', 'FrontController@join')->name('join');
});

// BACKEND
Route::group(['prefix' => 'dashboard','middleware' => 'auth'], function(){
	Route::get('/anggota/export', 'AnggotaController@export')->name('anggota.export');
	Route::post('/anggota/import', 'AnggotaController@import')->name('anggota.import');
	Route::resource('blogs','BlogsController');
	Route::resource('anggota','AnggotaController');
	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/settings/frontend', 'SetfrontController@index')->name('settings.frontend');

	//AJAX
	Route::get('/ajax_jk', 'DashboardController@ajax_jk')->name('ajax_jk');
	Route::get('/ajax_schedules', 'DashboardController@ajax_schedules')->name('ajax_schedules');
	Route::get('/ajax_delete_schedule/{id}', 'DashboardController@ajax_delete_schedule')->name('ajax_delete_schedule');
	Route::post('/ajax_add_schedule', 'DashboardController@ajax_add_schedule')->name('ajax_add_schedule');
	Route::post('/ajax_update_schedule/{id}', 'DashboardController@ajax_update_schedule')->name('ajax_update_schedule');

	//SLIDERS
	Route::get('/settings/sliders', 'SetfrontController@sliders')->name('settings.sliders');
	Route::post('/settings/create', 'SetfrontController@create_sliders')->name('settings.create');
	Route::post('/settings/sliders/delete/{id}', 'SetfrontController@delete_sliders')->name('settings.sliders.delete');
	Route::post('/sliders/edit', 'SetfrontController@edit_sliders')->name('sliders.edit');
	Route::get('/sliders/enable/{id}/{status}', 'SetfrontController@enable_sliders');

	//INFORMATIONS
	Route::get('/settings/informations', 'SetfrontController@informations')->name('settings.informations');

	Route::get('/clear-cache', function() {
		Artisan::call('cache:clear');
		return "Cache is cleared";
	});
});
